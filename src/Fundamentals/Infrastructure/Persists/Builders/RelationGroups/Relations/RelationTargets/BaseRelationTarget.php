<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\Table;
use LSVH\Lingo\Utilities\Parser;

abstract class BaseRelationTarget implements RelationTarget
{
    const PROP_TABLE = 'table';
    const PROP_COLUMN = 'column';

    private $table;

    private $column;

    public function __construct(Table $table, string $column)
    {
        $this->table = $table;
        $this->column = $column;
    }

    public function getTable(): Table
    {
        return $this->table;
    }

    public function getColumn(): string
    {
        return $this->column;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($instance) {
            $table = Parser::getArrayValue(self::PROP_TABLE, $instance);
            $column = Parser::getArrayValue(self::PROP_COLUMN, $instance);

            return new static($table, $column);
        }, $instances);
    }
}
