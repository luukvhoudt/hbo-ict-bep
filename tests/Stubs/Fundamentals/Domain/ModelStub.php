<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Model;

class ModelStub implements Model
{
    private $id;

    private $props;

    public function __construct(int $id, array $props)
    {
        $this->id = $id;
        $this->props = $props;
    }

    public function getIdentity(): ?int
    {
        return $this->id;
    }

    public static function getShortName(): string
    {
        return 'm';
    }

    public static function getPluralName(): string
    {
        return 'model';
    }

    public static function getFactory(): Factory
    {
        return new FactoryStub();
    }

    public function getAttributes(): array
    {
        return $this->props;
    }
}
