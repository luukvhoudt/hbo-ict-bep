<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\Structures\Structure;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLStructureGroup extends BaseStructureGroup
{
    public function toFormattedString(Formatter $formatter): string
    {
        return implode(';', array_map(function (Structure $column) use ($formatter) {
            return $column->toFormattedString($formatter);
        }, $this->getStructures()));
    }
}
