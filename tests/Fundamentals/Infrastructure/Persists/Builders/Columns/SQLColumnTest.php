<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\Columns;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\Columns\SQLColumn;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;

class SQLColumnTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $subject = new SQLColumn('foo', 'bar');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo` as `bar`', $actual);
    }

    /** @test */
    public function can_correctly_format_without_alias_to_string_for_store()
    {
        $subject = new SQLColumn('foo');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`', $actual);
    }

    /** @test */
    public function works_with_prefixed_columns()
    {
        $subject = new SQLColumn('foo.bar');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`.`bar`', $actual);
    }

    /** @test */
    public function works_with_wildcards()
    {
        $subject = new SQLColumn('*');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('*', $actual);
    }

    /** @test */
    public function works_with_prefixed_wildcards()
    {
        $subject = new SQLColumn('foo.*');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`.*', $actual);
    }
}
