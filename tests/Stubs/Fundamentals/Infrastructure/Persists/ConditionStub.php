<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\BaseCondition;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class ConditionStub extends BaseCondition
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'condition';
    }
}
