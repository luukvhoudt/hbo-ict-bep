<?php

namespace LSVH\Lingo\Components\Turns\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\Turn as TurnInterface;
use LSVH\Lingo\Utilities\Parser;

class TurnFactory extends BaseFactory
{
    const PROP_STARTED = Turn::PROP_STARTED;
    const PROP_STOPPED = Turn::PROP_STOPPED;
    const PROP_ANSWER = Turn::PROP_ANSWER;
    const PROP_HINTS = Turn::PROP_HINTS;

    public static function createInstance(array $props = []): TurnInterface
    {
        return new Turn(
            self::getIdentity(),
            self::getTimer($props),
            self::getAnswer($props),
            self::getHints($props)
        );
    }

    public static function loadInstance(int $id, array $props = []): TurnInterface
    {
        return new Turn(
            self::getIdentity($id),
            self::getTimer($props, 0),
            self::getAnswer($props),
            self::getHints($props, true)
        );
    }

    private static function getIdentity(int $id = null): TurnIdentity
    {
        return new TurnIdentity($id);
    }

    private static function getTimer(array $props, int $timerStartedDefault = null): TurnTimer
    {
        $timerStarted = Parser::getArrayValue(self::PROP_STARTED, $props, $timerStartedDefault);
        $timerAnswered = Parser::getArrayValue(self::PROP_STOPPED, $props);

        return new TurnTimer($timerStarted, $timerAnswered);
    }

    private static function getAnswer(array $props): TurnAnswer
    {
        $answerValue = Parser::getArrayValue(self::PROP_ANSWER, $props);

        return new TurnAnswer($answerValue);
    }

    private static function getHints(array $props, bool $load = false): array
    {
        $hints = Parser::getArrayValue(self::PROP_HINTS, $props, []);

        return self::createOrLoadInstancesWithFactory(TurnHintFactory::class, $hints, $load);
    }
}
