<?php

namespace LSVH\Lingo\Components\Turns\Domain;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseEntity;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Hint;
use LSVH\Lingo\Utilities\Guard;
use LSVH\Lingo\Utilities\Parser;

class TurnHint extends BaseEntity implements Hint
{
    const ABSENT = 'absent';
    const PRESENT = 'present';
    const CORRECT = 'correct';
    const TYPES = [self::ABSENT, self::PRESENT, self::CORRECT];
    const PROP_TYPE = 'type';
    const PROP_CHAR = 'char';

    public function __construct(int $id = null, array $props = [])
    {
        parent::__construct($id, $props);
        self::setupPropTypeGuards($props);
        self::setupPropCharGuards($props);
    }

    public static function getFactory(): Factory
    {
        return new TurnHintFactory();
    }

    public static function getShortName(): string
    {
        return 'th';
    }

    public static function getPluralName(): string
    {
        return 'turn hints';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID   => $this->getIdentity(),
            self::PROP_TYPE => $this->getType(),
            self::PROP_CHAR => $this->getChar(),
        ];
    }

    private static function setupPropTypeGuards(array $props): void
    {
        Guard::toRequireArrayKey(self::PROP_TYPE, $props);
        $type = $props[self::PROP_TYPE];
        Guard::valueToBeTypeOf($type, 'string');
        Guard::valueToBeOneOf($type, self::TYPES);
    }

    private static function setupPropCharGuards(array $props): void
    {
        Guard::toRequireArrayKey(self::PROP_CHAR, $props);
        $char = $props[self::PROP_CHAR];
        Guard::valueToBeTypeOf($char, 'string');
        Guard::stringValueToHaveLength($char, 1);
    }

    public function getType(): string
    {
        return Parser::getArrayValue(self::PROP_TYPE, $this->props, self::ABSENT);
    }

    public function getChar(): string
    {
        return Parser::getArrayValue(self::PROP_CHAR, $this->props);
    }
}
