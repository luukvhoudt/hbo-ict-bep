<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\SQLConditionGroup;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ConditionStub;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ConditionTargetStub;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ConditionValueStub;

class SQLConditionGroupTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $condition = new ConditionStub($target, $value);
        $subject = new SQLConditionGroup([$condition, $condition]);

        $actual = $subject->toFormattedString($this->formatter);
        $expected = $condition->toFormattedString($this->formatter);

        self::assertSame($expected.' and '.$expected, $actual);
    }

    /** @test */
    public function can_correctly_format_without_conditions_to_string_for_store()
    {
        $subject = new SQLConditionGroup([]);

        $actual = $subject->toFormattedString($this->formatter);

        self::assertEmpty($actual);
    }

    /** @test */
    public function can_correctly_format_with_different_operator_for_store()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $condition = new ConditionStub($target, $value);
        $subject = new SQLConditionGroup([$condition, $condition], 'or');

        $actual = $subject->toFormattedString($this->formatter);
        $expected = $condition->toFormattedString($this->formatter);

        self::assertSame($expected.' or '.$expected, $actual);
    }

    /** @test */
    public function uses_default_operator_when_non_existing_operator_is_given()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $condition = new ConditionStub($target, $value);
        $subject = new SQLConditionGroup([$condition, $condition], 'invalid');

        $actual = $subject->toFormattedString($this->formatter);
        $expected = $condition->toFormattedString($this->formatter);

        self::assertSame($expected.' and '.$expected, $actual);
    }

    /** @test */
    public function converts_operator_to_lowercase()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $condition = new ConditionStub($target, $value);
        $subject = new SQLConditionGroup([$condition, $condition], 'OR');

        $actual = $subject->toFormattedString($this->formatter);
        $expected = $condition->toFormattedString($this->formatter);

        self::assertSame($expected.' or '.$expected, $actual);
    }
}
