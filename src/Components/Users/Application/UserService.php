<?php

namespace LSVH\Lingo\Components\Users\Application;

use LSVH\Lingo\Fundamentals\Application\Services\BaseService;
use LSVH\Lingo\Fundamentals\Application\Services\Types\Create;
use LSVH\Lingo\Fundamentals\Domain\Models\Model;

class UserService extends BaseService implements Create
{
    public function create(array $props): Model
    {
        $model = $this->getRepository()::getFactory()::createInstance($props);

        return $this->getRepository()->insertOrUpdate($model);
    }
}
