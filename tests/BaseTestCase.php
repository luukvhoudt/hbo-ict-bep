<?php

namespace LSVH\Lingo\Tests;

use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected static function firstArrayItemAsKey(array $cases): array
    {
        $newCases = [];

        foreach ($cases as $case) {
            $firstArrayItem = current($case);
            $newCases[$firstArrayItem] = $case;
        }

        return $newCases;
    }
}
