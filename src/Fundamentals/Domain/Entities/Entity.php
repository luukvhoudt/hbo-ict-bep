<?php

namespace LSVH\Lingo\Fundamentals\Domain\Entities;

interface Entity extends Identity
{
    public function __construct(int $id = null, array $props = []);
}
