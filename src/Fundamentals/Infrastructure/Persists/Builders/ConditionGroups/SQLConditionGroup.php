<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLConditionGroup extends BaseConditionGroup
{
    public function toFormattedString(Formatter $formatter): string
    {
        return implode($this->formattedLogicalOperator(), array_map(function ($condition) use ($formatter) {
            if ($condition instanceof ConditionGroup) {
                return $this->formattedConditionGroup($condition, $formatter);
            }

            return $condition->toFormattedString($formatter);
        }, $this->getConditions()));
    }

    private function formattedConditionGroup(ConditionGroup $group, Formatter $formatter): string
    {
        return '('.$group->toFormattedString($formatter).')';
    }

    private function formattedLogicalOperator(): string
    {
        return ' '.$this->getLogicalOperator().' ';
    }
}
