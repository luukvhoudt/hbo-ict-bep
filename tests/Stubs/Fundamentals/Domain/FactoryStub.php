<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\Model;

class FactoryStub extends BaseFactory
{
    public static function createInstance(array $props = []): Model
    {
        return new ModelStub(null, $props);
    }

    public static function loadInstance(int $id, array $props = []): Model
    {
        return new ModelStub($id, $props);
    }
}
