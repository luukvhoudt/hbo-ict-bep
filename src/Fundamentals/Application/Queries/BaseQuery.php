<?php

namespace LSVH\Lingo\Fundamentals\Application\Queries;

use LSVH\Lingo\Fundamentals\Application\Services\Service;

abstract class BaseQuery implements Query
{
    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
}
