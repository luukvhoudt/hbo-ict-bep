<?php

namespace LSVH\Lingo\Utilities\Exceptions;

use Exception;

class ValueIsEmptyException extends Exception
{
    public function __construct($message, $code = 0, $previous = null)
    {
        $message = "Received an empty value `$message` when expecting the opposite.";
        parent::__construct($message, $code, $previous);
    }
}
