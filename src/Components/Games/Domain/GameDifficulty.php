<?php

namespace LSVH\Lingo\Components\Games\Domain;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseEntity;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Difficulty as DifficultyInterface;
use LSVH\Lingo\Fundamentals\Domain\Models\Level;
use LSVH\Lingo\Utilities\Guard;
use LSVH\Lingo\Utilities\Parser;

class GameDifficulty extends BaseEntity implements DifficultyInterface
{
    const PROP_NAME = 'name';
    const PROP_LEVELS = 'levels';

    public function __construct(int $id = null, array $props = [])
    {
        parent::__construct($id, $props);
        Guard::toRequireArrayKey(self::PROP_NAME, $props);
        Guard::valueToBeTypeOf($props[self::PROP_NAME], 'string');

        Guard::toRequireArrayKey(self::PROP_LEVELS, $props);
        $levels = $props[self::PROP_LEVELS];
        Guard::valueToBeTypeOf($levels, 'array');
        Guard::valueToNotBeEmpty($levels);
    }

    public static function getFactory(): Factory
    {
        return new GameDifficultyFactory();
    }

    public static function getShortName(): string
    {
        return 'gd';
    }

    public static function getPluralName(): string
    {
        return 'game difficulties';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID     => $this->getIdentity(),
            self::PROP_NAME   => $this->getName(),
            self::PROP_LEVELS => $this->getLevels(),
        ];
    }

    public function getName(): ?string
    {
        return Parser::getArrayValue(self::PROP_NAME, $this->props);
    }

    /**
     * @return Level[]
     */
    public function getLevels(): array
    {
        $levels = Parser::getArrayValue(self::PROP_LEVELS, $this->props, []);

        usort($levels, function (Level $a, Level $b) {
            return $a->getOrder() - $b->getOrder();
        });

        return $levels;
    }
}
