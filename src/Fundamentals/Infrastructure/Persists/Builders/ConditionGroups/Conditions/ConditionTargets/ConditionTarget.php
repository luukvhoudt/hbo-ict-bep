<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface ConditionTarget extends BuilderEntity
{
    public function __construct(string $column, string $table = null);

    public function getColumn(): string;

    public function getTable(): ?string;
}
