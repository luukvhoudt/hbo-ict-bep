<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets\ConditionTarget;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues\ConditionValue;
use LSVH\Lingo\Utilities\Parser;

abstract class BaseCondition implements Condition
{
    const PROP_TARGET = 'target';
    const PROP_VALUE = 'value';
    const PROP_COMPARISON_OPERATOR = 'comparisonOperator';

    private $target;

    private $value;

    private $comparisonOperator;

    public function __construct(
        ConditionTarget $target,
        ConditionValue $value,
        $comparisonOperator = self::DEFAULT_COMPARISON_OPERATOR
    ) {
        $this->target = $target;
        $this->value = $value;
        $this->comparisonOperator = in_array(strtolower($comparisonOperator), self::COMPARISON_OPERATORS)
            ? strtolower($comparisonOperator) : self::DEFAULT_COMPARISON_OPERATOR;
    }

    public function getTarget(): ConditionTarget
    {
        return $this->target;
    }

    public function getValue(): ConditionValue
    {
        return $this->value;
    }

    public function getComparisonOperator(): string
    {
        return $this->comparisonOperator;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        $defaultComparisonOperator = Parser::getArrayValue(self::PROP_COMPARISON_OPERATOR, $defaults);

        return array_map(function ($instance) use ($defaultComparisonOperator) {
            $target = Parser::getArrayValue(self::PROP_TARGET, $instance);
            $value = Parser::getArrayValue(self::PROP_VALUE, $instance);
            $comparisonOperator = Parser::getArrayValue(
                self::PROP_COMPARISON_OPERATOR,
                $instance,
                $defaultComparisonOperator
            );

            return new static($target, $value, $comparisonOperator);
        }, $instances);
    }
}
