<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results;

interface Result
{
    public function getRows(): array;
}
