<?php

namespace LSVH\Lingo\Components\Games\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\Level;
use LSVH\Lingo\Utilities\Parser;

class GameDifficultyLevelFactory extends BaseFactory
{
    const PROP_ORDER = GameDifficultyLevel::PROP_ORDER;
    const PROP_WORD_LENGTH = GameDifficultyLevel::PROP_WORD_LENGTH;

    public static function createInstance(array $props = []): Level
    {
        return new GameDifficultyLevel(null, self::getProps($props));
    }

    public static function loadInstance(int $id, array $props = []): Level
    {
        return new GameDifficultyLevel($id, self::getProps($props));
    }

    private static function getProps(array $props): array
    {
        return [
            GameDifficultyLevel::PROP_ORDER       => Parser::getArrayValue(self::PROP_ORDER, $props),
            GameDifficultyLevel::PROP_WORD_LENGTH => Parser::getArrayValue(self::PROP_WORD_LENGTH, $props),
        ];
    }
}
