<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\Relation;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLRelationGroup extends BaseRelationGroup
{
    public function toFormattedString(Formatter $formatter): string
    {
        return implode(' ', array_map(function (Relation $relation) use ($formatter) {
            return $relation->toFormattedString($formatter);
        }, $this->getRelations()));
    }
}
