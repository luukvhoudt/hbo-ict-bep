<?php

namespace LSVH\Lingo\Utilities\Exceptions;

use Exception;

class ValueIsTypeOfException extends Exception
{
    const PROP_VALUE = 'value';
    const PROP_TYPE = 'type';

    public function __construct($message, $code = 0, $previous = null)
    {
        $value = array_key_exists(self::PROP_VALUE, $message) ? $message[self::PROP_VALUE] : '';
        $expected = array_key_exists(self::PROP_TYPE, $message) ? $message[self::PROP_TYPE] : '';
        $actual = gettype($value);
        $message = "Incorrect type given, expected `$value` to be of type `$expected`, instead got `$actual`.";
        parent::__construct($message, $code, $previous);
    }
}
