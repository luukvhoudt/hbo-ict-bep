<?php

namespace LSVH\Lingo\Utilities\Exceptions;

use Exception;

class RequiredArrayKeyException extends Exception
{
    const PROP_KEY = 'key';
    const PROP_ARRAY = 'array';

    public function __construct($message, $code = 0, $previous = null)
    {
        $key = array_key_exists(self::PROP_KEY, $message) ? $message[self::PROP_KEY] : '';
        $array = array_key_exists(self::PROP_ARRAY, $message) ? $message[self::PROP_ARRAY] : [];
        $found = implode(', ', array_keys($array));
        $message = "Missing required array key `$key`, found the following keys: `$found`";
        parent::__construct($message, $code, $previous);
    }
}
