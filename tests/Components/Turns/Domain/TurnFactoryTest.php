<?php

namespace LSVH\Lingo\Tests\Components\Turns\Domain;

use LSVH\Lingo\Components\Turns\Domain\TurnFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class TurnFactoryTest extends BaseTestCase
{
    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = TurnFactory::createInstance();

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = TurnFactory::loadInstance(1);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            [], [],
        ];

        $actual = TurnFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            [TurnFactory::PROP_ID => 1], [TurnFactory::PROP_ID => 2],
        ];

        $actual = TurnFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }
}
