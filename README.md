# Lingo

My second attempt of realising an application in PHP, what allows people to train their "[Lingo](https://en.wikipedia.org/wiki/Lingo_(American_game_show))" skills.

## Summary
This repository contains a complete software system what is developed with the following mindset:

- Approach: _Domain Drive Development (DDD)_;
- Architecture: _Onion Hexagonal_;
- _Package by Component_;
- No strict dependencies on 3rd party code.

Thanks to the guidance of [this](https://herbertograca.com/2017/07/03/the-software-architecture-chronicles/) series of articles (especially [this article](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/)) I was able to maintain this mindset.

## History

The first version was made with another mindset. The mindset was more about getting things done as fast as possible.
However along the way it became clear that other things are also important. To have a look on version 1 please see
commit 6ee84b216b255f2580e1ef35532e9ef35e22cf57 (tip: click "Browse files"). As you can see the file structure is
completely different. This is because the first version was based on Laravel. The current version is completely
detached from any framework or whatsoever. 

The first version basically cost my whole holiday, to later come to the conclusion that the code became legacy code.
Resulting in starting over from scratch because there were some nasty bugs what where hard to debug as well as the code 
base itself was messy and tough to follow.
