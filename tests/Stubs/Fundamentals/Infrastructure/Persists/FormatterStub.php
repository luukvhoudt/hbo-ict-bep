<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class FormatterStub implements Formatter
{
    public function formatIdentifier(string $value): string
    {
        return '`'.$value.'`';
    }

    public function formatValue(string $value): string
    {
        return "'".$value."'";
    }
}
