<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\LaminasBuilder;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\LaminasFormatter;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Mappers\LaminasMapper;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores\LaminasStore;

class LaminasPersist extends BasePersist
{
    public function __construct()
    {
        $this->store = new LaminasStore();
        $this->mapper = new LaminasMapper();
        $this->builder = new LaminasBuilder(new LaminasFormatter());
    }
}
