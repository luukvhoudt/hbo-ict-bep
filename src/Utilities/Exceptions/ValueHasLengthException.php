<?php

namespace LSVH\Lingo\Utilities\Exceptions;

use Exception;

class ValueHasLengthException extends Exception
{
    const PROP_VALUE = 'value';
    const PROP_LENGTH = 'length';

    public function __construct($message, $code = 0, $previous = null)
    {
        $value = array_key_exists(self::PROP_VALUE, $message) ? $message[self::PROP_VALUE] : '';
        $expected = array_key_exists(self::PROP_LENGTH, $message) ? intval($message[self::PROP_LENGTH]) : 0;
        $actual = strlen($value);
        $message = "The length for `$value` is expected to be exactly `$expected`, instead got `$actual`.";
        parent::__construct($message, $code, $previous);
    }
}
