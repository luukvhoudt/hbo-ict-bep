<?php

namespace LSVH\Lingo\Components\Turns\Domain;

class TurnTimer
{
    /**
     * @var int
     */
    private $started;

    /**
     * @var int|null
     */
    private $stopped;

    public function __construct(int $started = null, int $stopped = null)
    {
        $this->started = $started === null ? time() : $started;
        $this->stopped = $stopped;
    }

    public function getStarted(): int
    {
        return $this->started;
    }

    public function getStopped(): ?int
    {
        return $this->stopped;
    }

    public function setStopped(?int $stopped): void
    {
        $this->stopped = $stopped;
    }
}
