<?php

namespace LSVH\Lingo\Tests\Components\Words\Domain;

use LSVH\Lingo\Components\Words\Domain\WordLanguageFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class WordLanguageFactoryTest extends BaseTestCase
{
    private $createProps;

    private $loadProps;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createProps = [
            WordLanguageFactory::PROP_CODE => 'en-US',
            WordLanguageFactory::PROP_NAME => 'English',
        ];
        $this->loadProps = array_merge([WordLanguageFactory::PROP_ID => 1], $this->createProps);
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = WordLanguageFactory::createInstance($this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = WordLanguageFactory::loadInstance(1, $this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            $this->loadProps,
            $this->loadProps,
            $this->loadProps,
        ];

        $actual = WordLanguageFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(3, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            $this->createProps,
            $this->createProps,
            $this->createProps,
        ];

        $actual = WordLanguageFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(3, $actual);
    }
}
