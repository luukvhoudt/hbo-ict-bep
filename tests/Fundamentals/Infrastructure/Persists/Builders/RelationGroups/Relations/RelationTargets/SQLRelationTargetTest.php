<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets\SQLRelationTarget;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\TableStub;

class SQLRelationTargetTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $subject = new SQLRelationTarget(new TableStub('hello'), 'foo');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`hello`.`foo`', $actual);
    }

    /** @test */
    public function can_correctly_format_with_table_alias_to_string_for_store()
    {
        $subject = new SQLRelationTarget(new TableStub('hello', 'world'), 'foo');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`world`.`foo`', $actual);
    }
}
