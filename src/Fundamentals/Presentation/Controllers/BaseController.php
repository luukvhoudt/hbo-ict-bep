<?php

namespace LSVH\Lingo\Fundamentals\Presentation\Controllers;

use LSVH\Lingo\Fundamentals\Presentation\Controllers\Requests\Request;
use LSVH\Lingo\Fundamentals\Presentation\Controllers\Responses\Response;

abstract class BaseController implements Controller
{
    protected $request;

    protected $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }
}
