<?php

namespace LSVH\Lingo\Fundamentals\Presentation\Controllers;

use LSVH\Lingo\Fundamentals\Presentation\Controllers\Requests\Request;
use LSVH\Lingo\Fundamentals\Presentation\Controllers\Responses\Response;

interface Controller
{
    public function __construct(Request $request, Response $response);
}
