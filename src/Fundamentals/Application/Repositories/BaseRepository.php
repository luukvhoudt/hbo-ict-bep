<?php

namespace LSVH\Lingo\Fundamentals\Application\Repositories;

use LSVH\Lingo\Fundamentals\Domain\Models\Model;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\ColumnGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\ConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\RelationGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Persist;

abstract class BaseRepository implements Repository
{
    protected $store;

    protected $mapper;

    protected $builder;

    public function __construct(Persist $persist)
    {
        $this->store = $persist->getStore();
        $this->mapper = $persist->getMapper();
        $this->builder = $persist->getBuilder();
    }

    public function batchInsert(array $columns, array $models): bool
    {
        $data = $this->mapper->toDataTransferObjects($models);

        if (empty($data)) {
            return false;
        }

        $command = $this->builder->insert(static::getTable(), $data, $columns);
        $rowCount = count($this->store->query($command)->getRows());

        return $rowCount === count($data);
    }

    public function insertOrUpdate(Model $model): ?Model
    {
        $id = $model->getIdentity();
        $data = $this->mapper->toDataTransferObject($model);
        $table = static::getTable();
        $columns = array_keys($data);
        $criteria = static::getConditionWhereIdEquals($id);
        $command = empty($id)
            ? $this->builder->insert($table, $data, $columns)
            : $this->builder->update($table, $data, $criteria);
        $rows = $this->store->query($command)->getRows();

        if (empty($rows)) {
            return null;
        }

        return current($model::getFactory()::loadInstances($rows));
    }

    public function select(
        ColumnGroup $columnGroup,
        RelationGroup $relationGroup = null,
        ConditionGroup $conditionGroup = null
    ): array {
        $command = $this->builder->select(static::getTable(), $columnGroup, $relationGroup, $conditionGroup);
        $rows = $this->store->query($command)->getRows();

        return $this->mapper->fromDataTransferObjects(static::getFactory(), $rows);
    }

    public function delete(ConditionGroup $conditionGroup): bool
    {
        $command = $this->builder->delete(static::getTable(), $conditionGroup);
        $rows = $this->store->query($command)->getRows();

        return !empty($rows);
    }

    public function setup(): bool
    {
        $command = $this->builder->setup(static::getStructureGroup());
        $rows = $this->store->query($command)->getRows();

        return !empty($rows);
    }
}
