<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\Columns;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;
use LSVH\Lingo\Utilities\Parser;

class SQLColumn extends BaseColumn
{
    public function toFormattedString(Formatter $formatter): string
    {
        $tableAndColumn = explode('.', $this->getIdentifierName());
        $tableOrColumn = Parser::getArrayValue(0, $tableAndColumn);
        $column = Parser::getArrayValue(1, $tableAndColumn);
        $formattedColumn = $this->formatColumnIdentifier(empty($column) ? $tableOrColumn : $column, $formatter);
        $formattedTable = !empty($column) ? $formatter->formatIdentifier($tableOrColumn) : null;
        $formattedIdentifierName = empty($formattedTable) ? $formattedColumn : $formattedTable.'.'.$formattedColumn;

        $unFormattedDisplayName = $this->getDisplayName();

        if (empty($unFormattedDisplayName)) {
            return $formattedIdentifierName;
        }

        return $formattedIdentifierName.' as '.$formatter->formatIdentifier($unFormattedDisplayName);
    }

    private function formatColumnIdentifier(string $column, Formatter $formatter): string
    {
        return $column === '*' ? $column : $formatter->formatIdentifier($column);
    }
}
