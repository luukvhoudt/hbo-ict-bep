<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

interface BuilderEntity
{
    public static function createInstances(array $instances, array $defaults = []): array;

    public function toFormattedString(Formatter $formatter): string;
}
