<?php

namespace LSVH\Lingo\Utilities;

abstract class Parser
{
    public static function getArrayValue(string $key, array $props, $default = null)
    {
        return array_key_exists($key, $props) ? $props[$key] : $default;
    }
}
