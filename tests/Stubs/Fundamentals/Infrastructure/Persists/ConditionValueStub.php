<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues\BaseConditionValue;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class ConditionValueStub extends BaseConditionValue
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'condition value';
    }
}
