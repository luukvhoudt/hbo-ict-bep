<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues;

use LSVH\Lingo\Utilities\Parser;

abstract class BaseConditionValue implements ConditionValue
{
    const PROP_VALUE = 'value';
    const PROP_ARRAY_HANDLER = 'arrayHandler';

    private $value;

    private $arrayHandler;

    public function __construct($value, callable $arrayHandler = null)
    {
        $this->value = $value;
        $this->arrayHandler = $arrayHandler;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function arrayToString(array $formattedValues): string
    {
        if (empty($this->arrayHandler)) {
            return '('.implode(',', $formattedValues).')';
        }

        return call_user_func($this->arrayHandler, $formattedValues);
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        $defaultArrayHandler = Parser::getArrayValue(self::PROP_ARRAY_HANDLER, $defaults);

        return array_map(function ($instance) use ($defaultArrayHandler) {
            if (is_array($instance)) {
                $value = Parser::getArrayValue(self::PROP_VALUE, $instance);
                $arrayHandler = Parser::getArrayValue(self::PROP_ARRAY_HANDLER, $instance, $defaultArrayHandler);

                return new static($value, $arrayHandler);
            }

            return new static($instance);
        }, $instances);
    }
}
