<?php

namespace LSVH\Lingo\Fundamentals\Domain\Entities;

interface Identity
{
    public function __construct(int $value = null);

    public function getIdentity(): ?int;
}
