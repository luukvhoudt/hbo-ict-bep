<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups;

abstract class BaseColumnGroup implements ColumnGroup
{
    private $columns;

    public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($instance) {
            return new static($instance);
        }, $instances);
    }
}
