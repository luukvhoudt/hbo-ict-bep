<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results;

use Laminas\Db\ResultSet\ResultSetInterface;

class LaminasResult implements Result
{
    private $result;

    public function __construct(ResultSetInterface $result)
    {
        $this->result = $result;
    }

    public function getRows(): array
    {
        return iterator_to_array($this->result);
    }
}
