<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\SQLCondition;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ConditionTargetStub;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ConditionValueStub;

class SQLConditionTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $subject = new SQLCondition($target, $value);

        $actual = $subject->toFormattedString($this->formatter);
        $expectedTarget = $target->toFormattedString($this->formatter);
        $expectedValue = $value->toFormattedString($this->formatter);

        self::assertSame($expectedTarget.' = '.$expectedValue, $actual);
    }

    /** @test */
    public function can_correctly_format_with_different_operator_to_string_for_store()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $subject = new SQLCondition($target, $value, '<');

        $actual = $subject->toFormattedString($this->formatter);
        $expectedTarget = $target->toFormattedString($this->formatter);
        $expectedValue = $value->toFormattedString($this->formatter);

        self::assertSame($expectedTarget.' < '.$expectedValue, $actual);
    }

    /** @test */
    public function uses_default_operator_when_non_existing_operator_is_given()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $subject = new SQLCondition($target, $value, 'invalid');

        $actual = $subject->toFormattedString($this->formatter);
        $expectedTarget = $target->toFormattedString($this->formatter);
        $expectedValue = $value->toFormattedString($this->formatter);

        self::assertSame($expectedTarget.' = '.$expectedValue, $actual);
    }

    /** @test */
    public function converts_operator_to_lowercase()
    {
        $target = new ConditionTargetStub('');
        $value = new ConditionValueStub('');
        $subject = new SQLCondition($target, $value, 'IN');

        $actual = $subject->toFormattedString($this->formatter);
        $expectedTarget = $target->toFormattedString($this->formatter);
        $expectedValue = $value->toFormattedString($this->formatter);

        self::assertSame($expectedTarget.' in '.$expectedValue, $actual);
    }
}
