<?php

namespace LSVH\Lingo\Components\Words\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\Word as WordInterface;
use LSVH\Lingo\Utilities\Parser;

class WordFactory extends BaseFactory
{
    const PROP_VALUE = Word::PROP_VALUE;
    const PROP_LANGUAGE = Word::PROP_LANGUAGE;

    public static function createInstance(array $props = []): WordInterface
    {
        return new Word(
            self::getIdentity(),
            self::getValue($props),
            self::getLanguage($props)
        );
    }

    public static function loadInstance(int $id, array $props = []): WordInterface
    {
        return new Word(
            self::getIdentity($id),
            self::getValue($props),
            self::getLanguage($props, true)
        );
    }

    private static function getIdentity(int $id = null): WordIdentity
    {
        return new WordIdentity($id);
    }

    private static function getValue(array $props): WordValue
    {
        $wordValue = Parser::getArrayValue(self::PROP_VALUE, $props);

        return new WordValue($wordValue);
    }

    private static function getLanguage(array $props, bool $load = false): WordLanguage
    {
        $lang = Parser::getArrayValue(self::PROP_LANGUAGE, $props, []);

        return self::createOrLoadInstanceWithFactory(WordLanguageFactory::class, $lang, $load);
    }
}
