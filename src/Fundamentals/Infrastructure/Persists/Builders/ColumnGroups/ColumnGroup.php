<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface ColumnGroup extends BuilderEntity
{
    public function __construct(array $columns);

    public function getColumns(): array;
}
