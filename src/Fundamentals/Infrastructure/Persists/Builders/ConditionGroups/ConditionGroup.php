<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface ConditionGroup extends BuilderEntity
{
    const LOGICAL_OPERATORS = ['and', 'or', 'not', 'and not', 'or not'];
    const DEFAULT_LOGICAL_OPERATOR = self::LOGICAL_OPERATORS[0];

    public function __construct(array $conditions, string $logicalOperator = self::DEFAULT_LOGICAL_OPERATOR);

    public function getConditions(): array;

    public function getLogicalOperator(): string;
}
