<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\Tables;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\SQLTable;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;

class SQLTableTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $subject = new SQLTable('foo', 'bar');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo` as `bar`', $actual);
    }

    /** @test */
    public function can_correctly_format_without_alias_to_string_for_store()
    {
        $subject = new SQLTable('foo');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`', $actual);
    }

    /** @test */
    public function converts_table_name_to_lower_case()
    {
        $subject = new SQLTable('FOO');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`', $actual);
    }

    /** @test */
    public function replaces_white_space_from_table_name_with_underscores()
    {
        $subject = new SQLTable('foo bar');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo_bar`', $actual);
    }

    /** @test */
    public function trims_table_name_its_surrounding_whitespace()
    {
        $subject = new SQLTable(" foo\t");

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`', $actual);
    }

    /** @test */
    public function converts_to_lower_trims_and_replaces_whitespace_with_underscores_from_table_name()
    {
        $subject = new SQLTable(" FOO BAR\t");

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo_bar`', $actual);
    }
}
