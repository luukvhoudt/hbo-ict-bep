<?php

namespace LSVH\Lingo\Tests\Stubs\Components\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Turn;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Domain\FactoryStub;
use LSVH\Lingo\Tests\Stubs\WithHistory;

class TurnStub implements Turn
{
    use WithHistory;

    public function getIdentity(): ?int
    {
        return null;
    }

    public static function getFactory(): Factory
    {
        return new FactoryStub();
    }

    public static function getShortName(): string
    {
        return '';
    }

    public static function getPluralName(): string
    {
        return '';
    }

    public function isActive(): bool
    {
        return true;
    }

    public function wasActive(): bool
    {
        return false;
    }

    public function isAnsweredCorrectly(): bool
    {
        return true;
    }

    public function submitAnswer(string $guessedAnswer): void
    {
        $this->addHistory('submitAnswer', [$guessedAnswer]);
    }

    public function validateAnswer(string $correctAnswer): void
    {
        $this->addHistory('validateAnswer', [$correctAnswer]);
    }

    public function getHints(): array
    {
        return [];
    }

    public function getAttributes(): array
    {
        return [];
    }
}
