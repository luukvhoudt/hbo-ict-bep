<?php

namespace LSVH\Lingo\Components\Games\Domain;

use LSVH\Lingo\Components\Rounds\Domain\RoundFactory;
use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\Game as GameInterface;
use LSVH\Lingo\Utilities\Parser;

class GameFactory extends BaseFactory
{
    const PROP_DIFFICULTY = Game::PROP_DIFFICULTY;
    const PROP_ROUNDS = Game::PROP_ROUNDS;

    public static function createInstance(array $props = []): GameInterface
    {
        return new Game(
            self::getIdentity(),
            self::getDifficulty($props),
            self::getRounds($props)
        );
    }

    public static function loadInstance(int $id, array $props = []): GameInterface
    {
        return new Game(
            self::getIdentity($id),
            self::getDifficulty($props, true),
            self::getRounds($props, true)
        );
    }

    private static function getIdentity(int $id = null): GameIdentity
    {
        return new GameIdentity($id);
    }

    private static function getDifficulty(array $props, bool $load = false): GameDifficulty
    {
        $difficulty = Parser::getArrayValue(self::PROP_DIFFICULTY, $props, []);

        return self::createOrLoadInstanceWithFactory(GameDifficultyFactory::class, $difficulty, $load);
    }

    private static function getRounds(array $props, bool $load = false): array
    {
        $rounds = Parser::getArrayValue(self::PROP_ROUNDS, $props, []);

        return self::createOrLoadInstancesWithFactory(RoundFactory::class, $rounds, $load);
    }
}
