<?php

namespace LSVH\Lingo\Tests\Stubs\Components\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Game;
use LSVH\Lingo\Fundamentals\Domain\Models\Level;
use LSVH\Lingo\Fundamentals\Domain\Models\Round;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Domain\FactoryStub;
use LSVH\Lingo\Tests\Stubs\WithHistory;

class GameStub implements Game
{
    use WithHistory;

    /**
     * @var int
     */
    private $score = 0;

    public function getIdentity(): ?int
    {
        return null;
    }

    public static function getFactory(): Factory
    {
        return new FactoryStub();
    }

    public static function getShortName(): string
    {
        return '';
    }

    public static function getPluralName(): string
    {
        return '';
    }

    public function isActive(): bool
    {
        return true;
    }

    public function wasActive(): bool
    {
        return false;
    }

    public function startOrContinueRound(array $props): void
    {
        $this->addHistory('startOrContinueRound', [$props]);
    }

    public function endRound(): void
    {
        $this->score = 10;
        $this->addHistory('endRound');
    }

    public function getActiveRound(): ?Round
    {
        return new RoundStub();
    }

    public function calculateScore(): int
    {
        return $this->score;
    }

    public function gaveOnlyCorrectAnswers(): bool
    {
        return true;
    }

    public function getActiveLevel(): Level
    {
        return new LevelStub();
    }

    public function getAttributes(): array
    {
        return [];
    }
}
