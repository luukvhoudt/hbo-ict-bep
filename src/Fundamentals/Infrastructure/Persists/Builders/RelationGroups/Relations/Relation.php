<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets\RelationTarget;

interface Relation extends BuilderEntity
{
    const STYLES = ['join', 'inner join', 'full outer join', 'left join', 'right join'];
    const DEFAULT_STYLE = self::STYLES[0];

    public function __construct(RelationTarget $base, RelationTarget $target, string $style = self::DEFAULT_STYLE);

    public function getBase(): RelationTarget;

    public function getTarget(): RelationTarget;

    public function getStyle(): string;
}
