<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Builder;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Mappers\Mapper;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores\Store;

abstract class BasePersist implements Persist
{
    protected $store;

    protected $builder;

    protected $mapper;

    public function getStore(): Store
    {
        return $this->store;
    }

    public function getBuilder(): Builder
    {
        return $this->builder;
    }

    public function getMapper(): Mapper
    {
        return $this->mapper;
    }
}
