<?php

namespace LSVH\Lingo\Fundamentals\Application\Services\Types;

use LSVH\Lingo\Fundamentals\Domain\Models\Model;

interface Create
{
    public function create(array $props): Model;
}
