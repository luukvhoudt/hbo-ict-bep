<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface Round extends Model, Activatable
{
    public function startOrContinueTurn(): void;

    public function endTurn(string $answer): void;

    public function getActiveTurn(): ?Turn;

    public function calculatePoints(): int;

    public function gaveCorrectAnswer(): bool;
}
