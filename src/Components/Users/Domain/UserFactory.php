<?php

namespace LSVH\Lingo\Components\Users\Domain;

use LSVH\Lingo\Components\Games\Domain\GameFactory;
use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\User as UserInterface;
use LSVH\Lingo\Utilities\Parser;

class UserFactory extends BaseFactory
{
    const PROP_USER = User::PROP_USER;
    const PROP_PASS = User::PROP_PASS;
    const PROP_GAMES = User::PROP_GAMES;

    public static function createInstance(array $props = []): UserInterface
    {
        return new User(
            self::getIdentity(),
            self::getLogin($props),
            self::getGames($props)
        );
    }

    public static function loadInstance(int $id, array $props = []): UserInterface
    {
        return new User(
            self::getIdentity($id),
            self::getLogin($props),
            self::getGames($props, true)
        );
    }

    private static function getIdentity(int $id = null): UserIdentity
    {
        return new UserIdentity($id);
    }

    private static function getLogin(array $props): UserLogin
    {
        $username = Parser::getArrayValue(self::PROP_USER, $props, []);
        $password = Parser::getArrayValue(self::PROP_PASS, $props, []);

        return new UserLogin($username, $password);
    }

    private static function getGames(array $props, bool $load = false): array
    {
        $games = Parser::getArrayValue(self::PROP_GAMES, $props, []);

        return self::createOrLoadInstancesWithFactory(GameFactory::class, $games, $load);
    }
}
