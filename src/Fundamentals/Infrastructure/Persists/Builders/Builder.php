<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\ColumnGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\ConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\RelationGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\StructureGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\Table;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

interface Builder
{
    public function __construct(Formatter $formatter);

    public function setup(StructureGroup $structures): string;

    public function insert(Table $table, array $data, array $column): string;

    public function select(
        Table $table,
        ColumnGroup $columnGroup,
        RelationGroup $relationGroup = null,
        ConditionGroup $conditionGroup = null
    ): string;

    public function update(Table $table, array $data, ConditionGroup $conditionGroup): string;

    public function delete(Table $table, ConditionGroup $conditionGroup): string;
}
