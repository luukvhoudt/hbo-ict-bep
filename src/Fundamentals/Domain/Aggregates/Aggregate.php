<?php

namespace LSVH\Lingo\Fundamentals\Domain\Aggregates;

use LSVH\Lingo\Fundamentals\Domain\Events\Event;
use LSVH\Lingo\Fundamentals\Domain\Subscribers\Subscriber;

interface Aggregate
{
    public function subscribe(Subscriber $subscriber): void;

    public function unsubscribe(Subscriber $subscriber): void;

    public function notify(Event $event): void;
}
