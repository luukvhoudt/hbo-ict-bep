<?php

namespace LSVH\Lingo\Components\Users\Domain;

use LSVH\Lingo\Components\Games\Domain\GameFactory;
use LSVH\Lingo\Fundamentals\Domain\Aggregates\BaseAggregate;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Game;
use LSVH\Lingo\Fundamentals\Domain\Models\User as UserInterface;

class User extends BaseAggregate implements UserInterface
{
    const PROP_ID = UserIdentity::PROP_ID;
    const PROP_USER = 'username';
    const PROP_PASS = 'password';
    const PROP_GAMES = 'games';

    /**
     * @var UserIdentity
     */
    private $id;

    /**
     * @var UserLogin
     */
    private $login;

    /**
     * @var Game[]
     */
    private $games;

    public function __construct(UserIdentity $id, UserLogin $login, array $games = [])
    {
        $this->id = $id;
        $this->login = $login;
        $this->games = $games;
    }

    public function getIdentity(): ?int
    {
        return $this->id->getIdentity();
    }

    public static function getFactory(): Factory
    {
        return new UserFactory();
    }

    public static function getShortName(): string
    {
        return 'u';
    }

    public static function getPluralName(): string
    {
        return 'users';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID    => $this->id->getIdentity(),
            self::PROP_USER  => $this->login->getUsername(),
            self::PROP_PASS  => $this->login->getPassword(),
            self::PROP_GAMES => $this->games,
        ];
    }

    public function startOrContinueGame(array $props): void
    {
        if (empty($this->games) || empty($this->getActiveGame())) {
            $this->games[] = GameFactory::createInstance($props);
        }
    }

    public function endGame(): void
    {
        $game = $this->getActiveGame();
        if (!empty($game)) {
            $game->endRound();
        }
    }

    public function getActiveGame(): ?Game
    {
        $activeGames = array_filter($this->games, function (Game $game) {
            return $game->isActive();
        });

        return empty($activeGames) ? null : current($activeGames);
    }
}
