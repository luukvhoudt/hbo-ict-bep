<?php

namespace LSVH\Lingo\Fundamentals\Presentation\Controllers\Requests;

interface Request
{
    public function __construct(string $path, array $params);

    public function getPath(): string;

    public function getParams(): array;

    public function getParamByKey(string $key): array;
}
