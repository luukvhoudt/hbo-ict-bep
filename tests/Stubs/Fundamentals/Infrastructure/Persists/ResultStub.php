<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results\Result;

class ResultStub implements Result
{
    public function getRows(): array
    {
        return [];
    }
}
