<?php

namespace LSVH\Lingo\Tests\Stubs\Components\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Level;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Domain\FactoryStub;

class LevelStub implements Level
{
    public function getIdentity(): ?int
    {
        return null;
    }

    public static function getFactory(): Factory
    {
        return new FactoryStub();
    }

    public static function getShortName(): string
    {
        return '';
    }

    public static function getPluralName(): string
    {
        return '';
    }

    public function getOrder(): int
    {
        return 1;
    }

    public function getWordLength(): int
    {
        return 2;
    }

    public function getAttributes(): array
    {
        return [];
    }
}
