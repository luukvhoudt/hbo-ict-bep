<?php

namespace LSVH\Lingo\Fundamentals\Application\Repositories;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Model;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\ColumnGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\ConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\RelationGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\StructureGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\Table;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Persist;

interface Repository
{
    public function __construct(Persist $persist);

    public function batchInsert(array $columns, array $models): bool;

    public function insertOrUpdate(Model $model): ?Model;

    public function select(
        ColumnGroup $columnGroup,
        RelationGroup $relationGroup = null,
        ConditionGroup $conditionGroup = null
    ): array;

    public function delete(ConditionGroup $conditionGroup): bool;

    public function setup(): bool;

    public static function getFactory(): Factory;

    public static function getTable(): Table;

    public static function getStructureGroup(): StructureGroup;

    public static function getConditionWhereIdEquals(int $value): ConditionGroup;
}
