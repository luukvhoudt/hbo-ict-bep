<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLTable extends BaseTable
{
    public function toFormattedString(Formatter $formatter): string
    {
        $formattedName = $formatter->formatIdentifier($this->getName());
        $unFormattedAlias = $this->getAlias();

        if (empty($unFormattedAlias)) {
            return $formattedName;
        }

        $formattedAlias = $formatter->formatIdentifier($unFormattedAlias);

        return implode(' ', [$formattedName, 'as', $formattedAlias]);
    }
}
