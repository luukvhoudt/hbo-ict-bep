<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface Table extends BuilderEntity
{
    public function __construct(string $name, string $alias = null);

    public function getName(): string;

    public function getAlias(): ?string;
}
