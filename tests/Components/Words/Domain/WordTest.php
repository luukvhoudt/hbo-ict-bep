<?php

namespace LSVH\Lingo\Tests\Components\Words\Domain;

use LSVH\Lingo\Components\Words\Domain\Word;
use LSVH\Lingo\Components\Words\Domain\WordIdentity;
use LSVH\Lingo\Components\Words\Domain\WordLanguage;
use LSVH\Lingo\Components\Words\Domain\WordValue;
use LSVH\Lingo\Components\Words\Exceptions\InvalidWordFormatException;
use LSVH\Lingo\Tests\BaseTestCase;

class WordTest extends BaseTestCase
{
    private $wordId;

    private $wordLanguage;

    protected function setUp(): void
    {
        parent::setUp();
        $this->wordId = new WordIdentity();
        $this->wordLanguage = new WordLanguage(null, [WordLanguage::PROP_CODE => 'en-US']);
    }

    /** @test */
    public function cannot_instantiate_with_an_invalid_word_format()
    {
        $this->expectException(InvalidWordFormatException::class);

        new Word($this->wordId, new WordValue('Hello'), $this->wordLanguage);
    }

    /** @test */
    public function can_instantiate_with_an_untrimmed_word()
    {
        $actual = new Word($this->wordId, new WordValue('!hello?'), $this->wordLanguage);

        self::assertSame('hello', $actual->getValue());
    }

    public function invalid_words()
    {
        return self::firstArrayItemAsKey([
            ['Hello'],
            ['WORLD'],
            ['hello!world'],
            ['hello@world'],
            ['hello#world'],
            ['hello$world'],
            ['hello^world'],
            ['hello&world'],
            ['hello*world'],
            ['hello(world'],
            ['hello)world'],
            ['hello-world'],
            ['hello_world'],
            ['hello+world'],
            ['hello=world'],
            ['hello~world'],
            ['hello`world'],
            ['hello<world'],
            ['hello>world'],
            ['hello,world'],
            ['hello.world'],
            ['hello/world'],
            ['hello?world'],
            ['hello:world'],
            ['hello;world'],
            ['hello\'world'],
            ['hello"world'],
            ['hello\world'],
            ['hello|world'],
            ['hello[world'],
            ['hello{world'],
            ['hello]world'],
            ['hello}world'],
        ]);
    }

    /**
     * @param string $word
     *
     * @test
     * @dataProvider invalid_words
     */
    public function does_only_accept_values_what_are_conform_the_format(string $word)
    {
        self::assertFalse(Word::isValidFormat($word));
    }

    public function untrimmed_words()
    {
        return self::firstArrayItemAsKey([
            ['!hello', 'hello'],
            ['!hello?', 'hello'],
            ['hello?', 'hello'],
            ['"hello"', 'hello'],
            ['\'hello\'', 'hello'],
            ['<hello>', 'hello'],
            ['{hello}', 'hello'],
            ['Hello', 'Hello'],
            ['hello', 'hello'],
            ['hello-world', 'hello-world'],
            ['!hello-world?', 'hello-world'],
        ]);
    }

    /**
     * @param string $untrimmed
     * @param string $trimmed
     *
     * @test
     * @dataProvider untrimmed_words
     */
    public function does_trim_words_from_invalid_characters(string $untrimmed, string $trimmed)
    {
        self::assertSame($trimmed, Word::trim($untrimmed));
    }
}
