<?php

namespace LSVH\Lingo\Fundamentals\Domain\Entities;

abstract class BaseIdentity implements Identity
{
    const PROP_ID = 'id';

    /**
     * @var int|null
     */
    protected $id;

    public function __construct(int $id = null)
    {
        $this->id = $id;
    }

    public function getIdentity(): ?int
    {
        return $this->id;
    }
}
