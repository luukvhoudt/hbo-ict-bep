<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\SQLRelation;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\RelationTargetStub;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\TableStub;

class SQLRelationTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $base = new RelationTargetStub(new TableStub('hello'), 'world');
        $target = new RelationTargetStub(new TableStub('foo'), 'bar');
        $subject = new SQLRelation($base, $target);

        $expected = 'join `foo` on relation target = relation target';

        self::assertSame($expected, $subject->toFormattedString($this->formatter));
    }

    /** @test */
    public function can_correctly_format_with_table_alias_to_string_for_store()
    {
        $base = new RelationTargetStub(new TableStub('hello'), 'world');
        $target = new RelationTargetStub(new TableStub('foo', 'f'), 'bar');
        $subject = new SQLRelation($base, $target);

        $expected = 'join `foo` `f` on relation target = relation target';

        self::assertSame($expected, $subject->toFormattedString($this->formatter));
    }

    /** @test */
    public function can_correctly_format_with_different_join_style_to_string_for_store()
    {
        $base = new RelationTargetStub(new TableStub('hello'), 'world');
        $target = new RelationTargetStub(new TableStub('foo'), 'bar');
        $subject = new SQLRelation($base, $target, 'right join');

        $expected = 'right join `foo` on relation target = relation target';

        self::assertSame($expected, $subject->toFormattedString($this->formatter));
    }

    /** @test */
    public function uses_default_join_style_when_not_in_styles_enum()
    {
        $base = new RelationTargetStub(new TableStub('hello'), 'world');
        $target = new RelationTargetStub(new TableStub('foo'), 'bar');
        $subject = new SQLRelation($base, $target, 'invalid join');

        $expected = 'join `foo` on relation target = relation target';

        self::assertSame($expected, $subject->toFormattedString($this->formatter));
    }
}
