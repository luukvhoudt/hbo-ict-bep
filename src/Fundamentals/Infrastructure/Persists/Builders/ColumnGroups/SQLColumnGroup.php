<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\Columns\Column;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLColumnGroup extends BaseColumnGroup
{
    public function toFormattedString(Formatter $formatter): string
    {
        return implode(',', array_map(function (Column $column) use ($formatter) {
            return $column->toFormattedString($formatter);
        }, $this->getColumns()));
    }
}
