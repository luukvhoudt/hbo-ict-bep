<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface Level extends Model
{
    public function getOrder(): int;

    public function getWordLength(): int;
}
