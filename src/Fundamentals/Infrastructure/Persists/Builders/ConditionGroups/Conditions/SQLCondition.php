<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLCondition extends BaseCondition
{
    public function toFormattedString(Formatter $formatter): string
    {
        $formattedTarget = $this->getTarget()->toFormattedString($formatter);
        $formattedValue = $this->getValue()->toFormattedString($formatter);
        $operator = $this->getComparisonOperator();

        return implode(' ', [$formattedTarget, $operator, $formattedValue]);
    }
}
