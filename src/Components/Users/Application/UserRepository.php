<?php

namespace LSVH\Lingo\Components\Users\Application;

use LSVH\Lingo\Components\Users\Domain\User;
use LSVH\Lingo\Components\Users\Domain\UserFactory;
use LSVH\Lingo\Fundamentals\Application\Repositories\BaseSQLRepository;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;

class UserRepository extends BaseSQLRepository
{
    const COLUMN_ID = User::PROP_ID;
    const COLUMN_USER = User::PROP_USER;
    const COLUMN_PASS = User::PROP_PASS;
    const COLUMN_AUTH = 'auth_token';
    const COLUMN_ACCESS = 'access_token';

    public static function getFactory(): Factory
    {
        return new UserFactory();
    }

    protected static function getTableName(): string
    {
        return User::getPluralName();
    }

    protected static function getTableAlias(): ?string
    {
        return User::getShortName();
    }

    protected static function getTableStructure(): array
    {
        return [
            self::COLUMN_USER   => self::CT_UNIQUE_STRING,
            self::COLUMN_PASS   => self::CT_REQUIRED_STRING,
            self::COLUMN_AUTH   => self::CT_STRING,
            self::COLUMN_ACCESS => self::CT_STRING,
        ];
    }
}
