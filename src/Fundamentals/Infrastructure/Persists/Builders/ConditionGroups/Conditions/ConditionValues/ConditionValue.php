<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface ConditionValue extends BuilderEntity
{
    public function __construct($value, callable $arrayHandler = null);

    public function getValue();

    public function arrayToString(array $formattedValues): string;
}
