<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\Table;

interface RelationTarget extends BuilderEntity
{
    public function __construct(Table $table, string $column);

    public function getTable(): Table;

    public function getColumn(): string;
}
