<?php

namespace LSVH\Lingo\Tests\Components\Words\Domain;

use LSVH\Lingo\Components\Words\Domain\WordFactory;
use LSVH\Lingo\Components\Words\Domain\WordLanguageFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class WordFactoryTest extends BaseTestCase
{
    private $wordLang;

    protected function setUp(): void
    {
        parent::setUp();

        $this->wordLang = [
            WordLanguageFactory::PROP_CODE => 'en-US',
            WordLanguageFactory::PROP_NAME => 'English',
        ];
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $props = [
            WordFactory::PROP_VALUE    => 'hello',
            WordFactory::PROP_LANGUAGE => $this->wordLang,
        ];

        $actual = WordFactory::createInstance($props);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $props = [
            WordFactory::PROP_VALUE    => 'hello',
            WordFactory::PROP_LANGUAGE => $this->wordLang,
        ];

        $actual = WordFactory::loadInstance(1, $props);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instances()
    {
        $values = [
            [
                WordFactory::PROP_VALUE    => 'hello',
                WordFactory::PROP_LANGUAGE => $this->wordLang,
            ],
            [
                WordFactory::PROP_VALUE    => 'world',
                WordFactory::PROP_LANGUAGE => $this->wordLang,
            ],
        ];

        $actual = WordFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }

    /** @test */
    public function can_create_multiple_instances_with_prop_default()
    {
        $expected = 'en-US';
        $values = [
            [
                WordFactory::PROP_VALUE => 'hello',
            ],
            [
                WordFactory::PROP_VALUE => 'world',
            ],
        ];

        $defaultValue = [
            WordFactory::PROP_LANGUAGE => $this->wordLang,
        ];

        $actual = WordFactory::createInstances($values, $defaultValue);

        self::assertSame($expected, $actual[0]->getLanguageCode());
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            [
                WordFactory::PROP_ID       => 1,
                WordFactory::PROP_VALUE    => 'hello',
                WordFactory::PROP_LANGUAGE => $this->wordLang,
            ],
            [
                WordFactory::PROP_ID       => 2,
                WordFactory::PROP_VALUE    => 'world',
                WordFactory::PROP_LANGUAGE => $this->wordLang,
            ],
        ];

        $actual = WordFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }
}
