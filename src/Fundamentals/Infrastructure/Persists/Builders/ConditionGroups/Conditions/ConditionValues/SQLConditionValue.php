<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets\ConditionTarget;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLConditionValue extends BaseConditionValue
{
    public function toFormattedString(Formatter $formatter): string
    {
        $v = $this->getValue();

        if ($v instanceof ConditionTarget) {
            return $v->toFormattedString($formatter);
        }

        if (is_array($v)) {
            return $this->arrayToString(array_map(function ($unFormattedValue) use ($formatter) {
                return $formatter->formatValue($unFormattedValue);
            }, $v));
        }

        return $formatter->formatValue($v);
    }
}
