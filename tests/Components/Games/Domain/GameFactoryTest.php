<?php

namespace LSVH\Lingo\Tests\Components\Games\Domain;

use LSVH\Lingo\Components\Games\Domain\GameDifficultyFactory;
use LSVH\Lingo\Components\Games\Domain\GameDifficultyLevelFactory;
use LSVH\Lingo\Components\Games\Domain\GameFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class GameFactoryTest extends BaseTestCase
{
    private $createProps;

    private $loadProps;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createProps = [
            GameFactory::PROP_DIFFICULTY => [
                GameDifficultyFactory::PROP_NAME   => 'hello',
                GameDifficultyFactory::PROP_LEVELS => [
                    [
                        GameDifficultyLevelFactory::PROP_ORDER       => 1,
                        GameDifficultyLevelFactory::PROP_WORD_LENGTH => 1,
                    ],
                ],
            ],
        ];
        $this->loadProps = [
            GameFactory::PROP_ID         => 1,
            GameFactory::PROP_DIFFICULTY => [
                GameDifficultyFactory::PROP_ID     => 1,
                GameDifficultyFactory::PROP_NAME   => 'hello',
                GameDifficultyFactory::PROP_LEVELS => [
                    [
                        GameDifficultyLevelFactory::PROP_ID          => 1,
                        GameDifficultyLevelFactory::PROP_ORDER       => 1,
                        GameDifficultyLevelFactory::PROP_WORD_LENGTH => 1,
                    ],
                ],
            ],
        ];
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = GameFactory::createInstance($this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = GameFactory::loadInstance(1, $this->loadProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            $this->createProps, $this->createProps,
        ];

        $actual = GameFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            $this->loadProps, $this->loadProps,
        ];

        $actual = GameFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }
}
