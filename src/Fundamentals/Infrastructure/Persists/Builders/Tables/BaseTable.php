<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables;

abstract class BaseTable implements Table
{
    const PROP_RELATIONAL = 'relational';

    private $name;

    private $alias;

    public function __construct(string $name, string $alias = null)
    {
        $this->name = self::formatName($name);
        $this->alias = $alias;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($nameOrIndex) use ($instances) {
            $nameOrAlias = $instances[$nameOrIndex];

            if (is_string($nameOrIndex)) {
                return new static($nameOrIndex, $nameOrAlias);
            }

            return new static($nameOrAlias);
        }, array_keys($instances));
    }

    private static function formatName(string $value): string
    {
        return preg_replace('/\s/', '_', trim(strtolower($value)));
    }
}
