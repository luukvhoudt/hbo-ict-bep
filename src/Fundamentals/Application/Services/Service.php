<?php

namespace LSVH\Lingo\Fundamentals\Application\Services;

use LSVH\Lingo\Fundamentals\Application\Repositories\Repository;

interface Service
{
    public function __construct(Repository $repository);

    public function getRepository(): Repository;
}
