<?php

namespace LSVH\Lingo\Components\Words\Domain;

use LSVH\Lingo\Components\Words\Exceptions\InvalidWordFormatException;
use LSVH\Lingo\Fundamentals\Domain\Aggregates\BaseAggregate;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Word as WordInterface;
use LSVH\Lingo\Utilities\EnvVars;

class Word extends BaseAggregate implements WordInterface
{
    const PROP_ID = WordIdentity::PROP_ID;
    const PROP_VALUE = 'value';
    const PROP_LANGUAGE = 'language';

    /**
     * @var WordIdentity
     */
    private $id;

    /**
     * @var WordValue
     */
    private $value;

    /**
     * @var WordLanguage
     */
    private $language;

    public function __construct(WordIdentity $id, WordValue $value, WordLanguage $language)
    {
        $trimmedValue = self::trim($value->getValue());
        if (!self::isValidFormat($trimmedValue)) {
            throw new InvalidWordFormatException($trimmedValue);
        }

        $this->id = $id;
        $this->value = new WordValue($trimmedValue);
        $this->language = $language;
    }

    public function getIdentity(): ?int
    {
        return $this->id->getIdentity();
    }

    public static function getFactory(): Factory
    {
        return new WordFactory();
    }

    public static function getShortName(): string
    {
        return 'w';
    }

    public static function getPluralName(): string
    {
        return 'words';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID       => $this->id->getIdentity(),
            self::PROP_VALUE    => $this->getValue(),
            self::PROP_LANGUAGE => $this->language,
        ];
    }

    public function getValue(): string
    {
        return $this->value->getValue();
    }

    public function getLength(): int
    {
        return strlen($this->getValue());
    }

    public function getLanguageCode(): string
    {
        return $this->language->getCode();
    }

    public static function trim(string $value): string
    {
        return preg_replace(EnvVars::getWordTrimRegex(), '', $value);
    }

    public static function isValidFormat(string $value): bool
    {
        return boolval(preg_match(EnvVars::getWordFormatRegex(), $value));
    }
}
