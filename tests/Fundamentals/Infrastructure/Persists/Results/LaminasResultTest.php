<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Results;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results\LaminasResult;
use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ResultSetStub;

class LaminasResultTest extends BaseTestCase
{
    /** @test */
    public function can_get_rows_correctly()
    {
        $resultSet = new ResultSetStub();
        $subject = new LaminasResult($resultSet);

        self::assertTrue(is_array($subject->getRows()));
    }

    /** @test */
    public function can_get_rows_with_data_correctly()
    {
        $expected = ['foo', 'bar'];

        $resultSet = new ResultSetStub($expected);
        $subject = new LaminasResult($resultSet);

        self::assertSame($expected, $subject->getRows());
    }
}
