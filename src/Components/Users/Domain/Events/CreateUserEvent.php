<?php

namespace LSVH\Lingo\Components\Users\Domain\Events;

use LSVH\Lingo\Fundamentals\Domain\Events\BaseEvent;

class CreateUserEvent extends BaseEvent
{
}
