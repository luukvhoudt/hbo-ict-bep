<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets\RelationTarget;
use LSVH\Lingo\Utilities\Parser;

abstract class BaseRelation implements Relation
{
    const PROP_BASE = 'base';
    const PROP_TARGET = 'target';
    const PROP_STYLE = 'style';

    private $base;

    private $target;

    private $style;

    public function __construct(RelationTarget $base, RelationTarget $target, string $style = self::DEFAULT_STYLE)
    {
        $this->base = $base;
        $this->target = $target;
        $this->style = in_array(strtolower($style), self::STYLES) ? strtolower($style) : self::DEFAULT_STYLE;
    }

    public function getBase(): RelationTarget
    {
        return $this->base;
    }

    public function getTarget(): RelationTarget
    {
        return $this->target;
    }

    public function getStyle(): string
    {
        return $this->style;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        $defaultStyle = Parser::getArrayValue(self::PROP_STYLE, $defaults);

        return array_map(function ($instance) use ($defaultStyle) {
            $baseTable = Parser::getArrayValue(self::PROP_BASE, $instance);
            $targetTable = Parser::getArrayValue(self::PROP_TARGET, $instance);
            $style = Parser::getArrayValue(self::PROP_STYLE, $instance, $defaultStyle);

            return new static($baseTable, $targetTable, $style);
        }, $instances);
    }
}
