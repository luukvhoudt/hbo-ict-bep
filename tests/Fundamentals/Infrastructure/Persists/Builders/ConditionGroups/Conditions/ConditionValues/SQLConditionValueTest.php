<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues\SQLConditionValue;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\ConditionTargetStub;

class SQLConditionValueTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $subject = new SQLConditionValue('foo');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame("'foo'", $actual);
    }

    /** @test */
    public function can_correctly_format_condition_target_as_value_to_string_for_store()
    {
        $target = new ConditionTargetStub('');
        $subject = new SQLConditionValue($target);

        $actual = $subject->toFormattedString($this->formatter);

        $expected = $target->toFormattedString($this->formatter);

        self::assertSame($expected, $actual);
    }

    /** @test */
    public function can_correctly_format_array_as_value_to_string_for_store()
    {
        $subject = new SQLConditionValue(['foo', 'bar']);

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame("('foo','bar')", $actual);
    }

    /** @test */
    public function can_influence_how_an_array_as_value_is_formatted()
    {
        $subject = new SQLConditionValue(['foo', 'bar'], function ($values): string {
            return implode(' and ', $values);
        });

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame("'foo' and 'bar'", $actual);
    }
}
