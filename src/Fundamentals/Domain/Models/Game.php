<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface Game extends Model, Activatable
{
    public function startOrContinueRound(array $props): void;

    public function endRound(): void;

    public function getActiveRound(): ?Round;

    public function calculateScore(): int;

    public function gaveOnlyCorrectAnswers(): bool;

    public function getActiveLevel(): Level;
}
