<?php

namespace LSVH\Lingo\Fundamentals\Domain\Factories;

interface Factory
{
    public static function createInstance(array $props = []);

    public static function createInstances(array $values = []): array;

    public static function loadInstance(int $id, array $props = []);

    public static function loadInstances(array $values): array;
}
