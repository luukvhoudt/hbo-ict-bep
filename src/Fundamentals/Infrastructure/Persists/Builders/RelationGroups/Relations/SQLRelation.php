<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLRelation extends BaseRelation
{
    public function toFormattedString(Formatter $formatter): string
    {
        $base = $this->getBase()->toFormattedString($formatter);
        $target = $this->getTarget()->toFormattedString($formatter);

        $targetTable = $this->getTarget()->getTable();
        $targetTableAlias = $targetTable->getAlias();
        $formattedTargetTableName = $formatter->formatIdentifier($targetTable->getName());
        $formattedTargetTableAlias = empty($targetTableAlias) ? '' : $formatter->formatIdentifier($targetTableAlias);
        $subject = $formattedTargetTableName.(empty($formattedTargetTableAlias) ? '' : ' '.$formattedTargetTableAlias);

        $style = $this->getStyle();

        return implode(' ', [$style, $subject, 'on', $base, '=', $target]);
    }
}
