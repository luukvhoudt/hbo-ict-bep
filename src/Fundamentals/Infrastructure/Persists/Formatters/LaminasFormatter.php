<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters;

use Laminas\Db\Adapter\Adapter;
use LSVH\Lingo\Utilities\EnvVars;

class LaminasFormatter implements Formatter
{
    private $platform;

    public function __construct()
    {
        $this->platform = (new Adapter([
            'driver'   => EnvVars::getDatabaseDriver(),
            'platform' => EnvVars::getDatabasePlatform(),
        ]))->getPlatform();
    }

    public function formatIdentifier(string $value): string
    {
        return $this->platform->quoteIdentifier($value);
    }

    public function formatValue(string $value): string
    {
        return $this->platform->quoteValue($value);
    }
}
