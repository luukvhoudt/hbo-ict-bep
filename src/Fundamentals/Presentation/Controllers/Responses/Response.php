<?php

namespace LSVH\Lingo\Fundamentals\Presentation\Controllers\Responses;

interface Response
{
    public function __construct($value);

    public function setValue($value): void;

    public function getValue();
}
