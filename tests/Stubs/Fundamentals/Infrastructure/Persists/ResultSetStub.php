<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use Iterator;
use Laminas\Db\ResultSet\ResultSetInterface;

class ResultSetStub implements Iterator, ResultSetInterface
{
    private $dataSource;

    public function __construct(array $dataSource = [])
    {
        $this->dataSource = $dataSource;
    }

    public function initialize($dataSource)
    {
        return $this;
    }

    public function getFieldCount()
    {
        return count($this->dataSource);
    }

    public function current()
    {
        return current($this->dataSource);
    }

    public function count()
    {
        return count($this->dataSource);
    }

    public function key()
    {
        return key($this->dataSource);
    }

    public function valid()
    {
        return key($this->dataSource) !== null;
    }

    public function next()
    {
        next($this->dataSource);
    }

    public function rewind()
    {
        reset($this->dataSource);
    }
}
