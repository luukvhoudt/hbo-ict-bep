<?php

namespace LSVH\Lingo\Tests\Components\Turns\Domain;

use LSVH\Lingo\Components\Turns\Domain\TurnHint;
use LSVH\Lingo\Components\Turns\Domain\TurnHintFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class TurnHintFactoryTest extends BaseTestCase
{
    private $createProps;

    private $loadProps;

    protected function setUp(): void
    {
        parent::setUp();
        $this->createProps = [
            TurnHintFactory::PROP_TYPE => TurnHint::ABSENT,
            TurnHintFactory::PROP_CHAR => 'a',
        ];
        $this->loadProps = array_merge([TurnHintFactory::PROP_ID => 1], $this->createProps);
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = TurnHintFactory::createInstance($this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = TurnHintFactory::loadInstance(1, $this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            $this->loadProps,
            $this->loadProps,
            $this->loadProps,
        ];

        $actual = TurnHintFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(3, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            $this->createProps,
            $this->createProps,
            $this->createProps,
        ];

        $actual = TurnHintFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(3, $actual);
    }
}
