<?php

namespace LSVH\Lingo\Tests\Components\Games\Domain;

use LSVH\Lingo\Components\Games\Domain\GameDifficultyLevelFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class GameDifficultyLevelFactoryTest extends BaseTestCase
{
    private $createProps;

    private $loadProps;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createProps = [
            GameDifficultyLevelFactory::PROP_ORDER       => 1,
            GameDifficultyLevelFactory::PROP_WORD_LENGTH => 1,
        ];
        $this->loadProps = [
            GameDifficultyLevelFactory::PROP_ID          => 1,
            GameDifficultyLevelFactory::PROP_ORDER       => 1,
            GameDifficultyLevelFactory::PROP_WORD_LENGTH => 1,
        ];
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = GameDifficultyLevelFactory::createInstance($this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = GameDifficultyLevelFactory::loadInstance(1, $this->loadProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            $this->createProps, $this->createProps,
        ];

        $actual = GameDifficultyLevelFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            $this->loadProps, $this->loadProps,
        ];

        $actual = GameDifficultyLevelFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }
}
