<?php

namespace LSVH\Lingo\Components\Turns\Domain;

use LSVH\Lingo\Components\Turns\Domain\Events\ShouldCalculateHintsEvent;
use LSVH\Lingo\Fundamentals\Domain\Aggregates\BaseAggregate;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Turn as TurnInterface;
use LSVH\Lingo\Utilities\EnvVars;

class Turn extends BaseAggregate implements TurnInterface
{
    const PROP_ID = TurnIdentity::PROP_ID;
    const PROP_STARTED = 'started';
    const PROP_STOPPED = 'stopped';
    const PROP_ANSWER = 'answer';
    const PROP_HINTS = 'hints';

    /**
     * @var TurnIdentity
     */
    private $id;

    /**
     * @var TurnTimer
     */
    private $timer;

    /**
     * @var TurnAnswer
     */
    private $answer;

    /**
     * @var TurnHint[]
     */
    private $hints;

    public function __construct(TurnIdentity $id, TurnTimer $timer, TurnAnswer $answer, array $hints = [])
    {
        $this->id = $id;
        $this->timer = $timer;
        $this->answer = $answer;
        $this->hints = $hints;
    }

    public function getIdentity(): ?int
    {
        return $this->id->getIdentity();
    }

    public static function getFactory(): Factory
    {
        return new TurnFactory();
    }

    public static function getShortName(): string
    {
        return 't';
    }

    public static function getPluralName(): string
    {
        return 'turns';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID      => $this->id->getIdentity(),
            self::PROP_STARTED => $this->timer->getStarted(),
            self::PROP_STOPPED => $this->timer->getStopped(),
            self::PROP_ANSWER  => $this->answer->getValue(),
            self::PROP_HINTS   => $this->hints,
        ];
    }

    public function isActive(): bool
    {
        return $this->answer->getValue() === null;
    }

    public function wasActive(): bool
    {
        return !$this->isActive();
    }

    public function isAnsweredCorrectly(): bool
    {
        return $this->wasActive() && $this->isAnsweredOnTime() && empty($this->hints);
    }

    private function isAnsweredOnTime(): bool
    {
        $answeredAt = $this->timer->getStopped() == null ? time() : $this->timer->getStopped();

        return $answeredAt <= $this->timer->getStarted() + EnvVars::getTurnDuration();
    }

    public function submitAnswer(string $guessedAnswer): void
    {
        if ($this->isActive()) {
            $this->timer->setStopped(time());
            $this->answer->setValue($guessedAnswer);
        }
    }

    public function validateAnswer(string $correctAnswer): void
    {
        $guessedAnswer = $this->answer->getValue();
        if ($this->wasActive() && $correctAnswer != $guessedAnswer) {
            $event = new ShouldCalculateHintsEvent($this->id->getIdentity(), [$guessedAnswer]);
            $this->notify($event);
            if (boolval($event->getPayload())) {
                $this->calculateHints($correctAnswer);
            }
        }
    }

    private function calculateHints(string $correctAnswer): void
    {
        $guessedAnswerChars = str_split($this->answer->getValue());

        $calculatedHints = [];
        foreach ($guessedAnswerChars as $charIndex => $char) {
            $calculatedHints[] = [
                TurnHint::PROP_TYPE => $this->calculateHintType($correctAnswer, $char, $charIndex),
                TurnHint::PROP_CHAR => $char,
            ];
        }

        $this->hints = TurnHintFactory::createInstances($calculatedHints);
    }

    private function calculateHintType(string $correctAnswer, $char, int $charIndex): string
    {
        $correctAnswerChars = str_split($correctAnswer);

        if (!in_array($char, $correctAnswerChars)) {
            return TurnHint::ABSENT;
        }

        if (substr($correctAnswer, $charIndex, 1) === $char) {
            return TurnHint::CORRECT;
        }

        return TurnHint::PRESENT;
    }

    public function getHints(): array
    {
        return $this->hints;
    }
}
