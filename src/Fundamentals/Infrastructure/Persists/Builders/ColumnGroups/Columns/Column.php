<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\Columns;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface Column extends BuilderEntity
{
    public function __construct(string $identifierName, string $displayName = null);

    public function getIdentifierName(): string;

    public function getDisplayName(): ?string;
}
