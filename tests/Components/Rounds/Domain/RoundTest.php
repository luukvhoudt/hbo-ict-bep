<?php

namespace LSVH\Lingo\Tests\Components\Rounds\Domain;

use LSVH\Lingo\Components\Rounds\Domain\Round;
use LSVH\Lingo\Components\Rounds\Domain\RoundIdentity;
use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Tests\Stubs\Components\Domain\WordStub;

class RoundTest extends BaseTestCase
{
    private $roundId;

    private $word;

    private $round;

    protected function setUp(): void
    {
        parent::setUp();
        $this->roundId = new RoundIdentity();
        $this->word = new WordStub();
        $this->round = new Round($this->roundId, $this->word);
    }

    /** @test */
    public function is_active_initially()
    {
        self::assertTrue($this->round->isActive());
    }

    /** @test */
    public function should_start_a_turn_without_any_turns_specified_yet()
    {
        self::assertEmpty($this->round->getActiveTurn());

        $this->round->startOrContinueTurn();

        self::assertNotEmpty($this->round->getActiveTurn());
    }

    /** @test */
    public function should_not_start_another_turn_when_one_is_already_active()
    {
        $expected = 40;

        $this->round->startOrContinueTurn();
        self::assertNotEmpty($this->round->getActiveTurn());
        self::assertSame($expected, $this->round->calculatePoints());

        $this->round->startOrContinueTurn();
        self::assertSame($expected, $this->round->calculatePoints());
    }

    /** @test */
    public function should_end_only_the_active_turn()
    {
        $this->round->startOrContinueTurn();
        $actual = $this->round->getActiveTurn();

        $this->round->endTurn('world');

        self::assertTrue($actual->wasActive());
    }

    /** @test */
    public function subtracts_points_for_every_failed_turn()
    {
        $initially = $this->round->calculatePoints();
        $this->round->startOrContinueTurn();
        $this->round->endTurn('world');

        $afterOneTurn = $this->round->calculatePoints();

        self::assertLessThan($initially, $afterOneTurn);
    }

    /** @test */
    public function becomes_inactive_when_points_reach_zero()
    {
        while ($this->round->calculatePoints() > 0) {
            $this->round->startOrContinueTurn();
            $this->round->endTurn('world');
        }

        self::assertTrue($this->round->wasActive());
    }

    /** @test */
    public function becomes_inactive_when_answered_correctly()
    {
        $this->round->startOrContinueTurn();
        $this->round->endTurn(WordStub::VALUE);

        self::assertTrue($this->round->wasActive());
    }
}
