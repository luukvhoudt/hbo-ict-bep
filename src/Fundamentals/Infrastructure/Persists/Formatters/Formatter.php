<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters;

interface Formatter
{
    public function formatIdentifier(string $value): string;

    public function formatValue(string $value): string;
}
