<?php

namespace LSVH\Lingo\Components\Games\Domain;

use LSVH\Lingo\Components\Rounds\Domain\RoundFactory;
use LSVH\Lingo\Fundamentals\Domain\Aggregates\BaseAggregate;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Game as GameInterface;
use LSVH\Lingo\Fundamentals\Domain\Models\Level;
use LSVH\Lingo\Fundamentals\Domain\Models\Round;

class Game extends BaseAggregate implements GameInterface
{
    const PROP_ID = GameIdentity::PROP_ID;
    const PROP_DIFFICULTY = 'difficulty';
    const PROP_ROUNDS = 'rounds';

    /**
     * @var GameIdentity
     */
    private $id;

    /**
     * @var GameDifficulty
     */
    private $difficulty;

    /**
     * @var Round[]
     */
    private $rounds;

    public function __construct(GameIdentity $id, GameDifficulty $difficulty, array $rounds = [])
    {
        $this->id = $id;
        $this->difficulty = $difficulty;
        $this->rounds = $rounds;
    }

    public function getIdentity(): ?int
    {
        return $this->id->getIdentity();
    }

    public static function getFactory(): Factory
    {
        return new GameFactory();
    }

    public static function getShortName(): string
    {
        return 'g';
    }

    public static function getPluralName(): string
    {
        return 'games';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID         => $this->id->getIdentity(),
            self::PROP_DIFFICULTY => $this->difficulty,
            self::PROP_ROUNDS     => $this->rounds,
        ];
    }

    public function isActive(): bool
    {
        $round = $this->getActiveRound();

        return empty($this->rounds) || (!empty($round) && $round->isActive()) || $this->gaveOnlyCorrectAnswers();
    }

    public function wasActive(): bool
    {
        return !$this->isActive();
    }

    public function getActiveRound(): ?Round
    {
        $activeRounds = array_filter($this->rounds, function (Round $round) {
            return $round->isActive();
        });

        return empty($activeRounds) ? null : current($activeRounds);
    }

    public function startOrContinueRound(array $props): void
    {
        if (empty($this->getActiveRound()) && $this->gaveOnlyCorrectAnswers()) {
            $this->rounds[] = RoundFactory::createInstance($props);
        }
    }

    public function endRound(): void
    {
        $round = $this->getActiveRound();
        if ($this->isActive() && !empty($round)) {
            while ($round->calculatePoints() > 0) {
                $round->startOrContinueTurn();
                $round->endTurn('-');
            }
        }
    }

    public function calculateScore(): int
    {
        return array_sum(array_map(function (Round $round) {
            return $round->calculatePoints();
        }, $this->rounds));
    }

    public function gaveOnlyCorrectAnswers(): bool
    {
        $roundsCorrectlyAnswered = array_filter($this->rounds, function (Round $round) {
            return $round->gaveCorrectAnswer();
        });

        return count($this->rounds) === count($roundsCorrectlyAnswered);
    }

    public function getActiveLevel(): Level
    {
        $levels = $this->difficulty->getLevels();
        $remainder = count($this->rounds) % count($levels);
        $index = $remainder === 0 ? count($levels) : $remainder;

        return $levels[$index - 1];
    }
}
