<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\BaseRelationGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class RelationGroupStub extends BaseRelationGroup
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'relation group';
    }
}
