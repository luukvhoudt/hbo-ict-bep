<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results\Result;

interface Store
{
    public function query(string $statement): Result;
}
