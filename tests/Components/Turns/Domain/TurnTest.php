<?php

namespace LSVH\Lingo\Tests\Components\Turns\Domain;

use LSVH\Lingo\Components\Turns\Domain\Turn;
use LSVH\Lingo\Components\Turns\Domain\TurnAnswer;
use LSVH\Lingo\Components\Turns\Domain\TurnHint;
use LSVH\Lingo\Components\Turns\Domain\TurnIdentity;
use LSVH\Lingo\Components\Turns\Domain\TurnTimer;
use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Utilities\EnvVars;

class TurnTest extends BaseTestCase
{
    private $turnId;

    private $turnTimer;

    private $turnAnswer;

    private $turnHints;

    private $turn;

    protected function setUp(): void
    {
        parent::setUp();
        $this->turnId = new TurnIdentity();
        $this->turnTimer = new TurnTimer();
        $this->turnAnswer = new TurnAnswer();
        $this->turnHints = [
            new TurnHint(null, [TurnHint::PROP_CHAR => 'a', TurnHint::PROP_TYPE => TurnHint::ABSENT]),
            new TurnHint(null, [TurnHint::PROP_CHAR => 'b', TurnHint::PROP_TYPE => TurnHint::PRESENT]),
            new TurnHint(null, [TurnHint::PROP_CHAR => 'c', TurnHint::PROP_TYPE => TurnHint::CORRECT]),
        ];
        $this->turn = new Turn($this->turnId, $this->turnTimer, $this->turnAnswer);
    }

    /** @test */
    public function can_initialize_with_hints()
    {
        $turn = new Turn($this->turnId, $this->turnTimer, $this->turnAnswer, $this->turnHints);

        self::assertNotEmpty($turn->getHints());
        self::assertCount(count($this->turnHints), $turn->getHints());
    }

    /** @test */
    public function should_be_answered_incorrect_when_initialized_with_hints()
    {
        $turn = new Turn($this->turnId, $this->turnTimer, $this->turnAnswer, $this->turnHints);

        self::assertFalse($turn->isAnsweredCorrectly());
    }

    /** @test */
    public function should_be_answered_incorrect_when_answered_to_late()
    {
        $timer = new TurnTimer(0);
        $turn = new Turn($this->turnId, $timer, $this->turnAnswer);

        $turn->submitAnswer('foo');

        self::assertFalse($turn->isAnsweredCorrectly());
    }

    /** @test */
    public function should_be_answered_just_within_the_time_limits()
    {
        $timer = new TurnTimer(time() - EnvVars::getTurnDuration());
        $turn = new Turn($this->turnId, $timer, $this->turnAnswer);

        $turn->submitAnswer('foo');

        self::assertTrue($turn->isAnsweredCorrectly());
    }

    /** @test */
    public function should_be_answered_just_outside_of_the_time_limits()
    {
        $timer = new TurnTimer(time() - (EnvVars::getTurnDuration() + 1));
        $turn = new Turn($this->turnId, $timer, $this->turnAnswer);

        $turn->submitAnswer('foo');

        self::assertFalse($turn->isAnsweredCorrectly());
    }

    /** @test */
    public function should_hint_about_absent_chars()
    {
        $this->turn->submitAnswer('a');
        $this->turn->validateAnswer('b');

        $actual = $this->turn->getHints();

        self::assertCount(1, $actual);
        self::assertSame(TurnHint::ABSENT, $actual[0]->getType());
    }

    /** @test */
    public function should_hint_about_present_chars()
    {
        $this->turn->submitAnswer('b');
        $this->turn->validateAnswer('ab');

        $actual = $this->turn->getHints();

        self::assertCount(1, $actual);
        self::assertSame(TurnHint::PRESENT, $actual[0]->getType());
    }

    /** @test */
    public function should_hint_about_correct_chars()
    {
        $this->turn->submitAnswer('a');
        $this->turn->validateAnswer('ab');

        $actual = $this->turn->getHints();

        self::assertCount(1, $actual);
        self::assertSame(TurnHint::CORRECT, $actual[0]->getType());
    }
}
