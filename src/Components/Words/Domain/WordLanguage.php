<?php

namespace LSVH\Lingo\Components\Words\Domain;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseEntity;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Language as LanguageInterface;
use LSVH\Lingo\Utilities\Guard;
use LSVH\Lingo\Utilities\Parser;

class WordLanguage extends BaseEntity implements LanguageInterface
{
    const PROP_CODE = 'code';
    const PROP_NAME = 'name';

    public function __construct(int $id = null, array $props = [])
    {
        parent::__construct($id, $props);
        Guard::toRequireArrayKey(self::PROP_CODE, $props);
        Guard::valueToBeTypeOf($props[self::PROP_CODE], 'string');
    }

    public static function getFactory(): Factory
    {
        return new WordLanguageFactory();
    }

    public static function getShortName(): string
    {
        return 'wl';
    }

    public static function getPluralName(): string
    {
        return 'word_languages';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID   => $this->getIdentity(),
            self::PROP_CODE => $this->getCode(),
            self::PROP_NAME => $this->getName(),
        ];
    }

    public function getCode(): string
    {
        return Parser::getArrayValue(self::PROP_CODE, $this->props);
    }

    public function getName(): ?string
    {
        return Parser::getArrayValue(self::PROP_NAME, $this->props);
    }
}
