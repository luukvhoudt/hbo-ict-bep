<?php

namespace LSVH\Lingo\Components\Words\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Utilities\Parser;

class WordLanguageFactory extends BaseFactory
{
    const PROP_CODE = WordLanguage::PROP_CODE;
    const PROP_NAME = WordLanguage::PROP_NAME;

    public static function createInstance(array $props = [])
    {
        return new WordLanguage(null, self::getProps($props));
    }

    public static function loadInstance(int $id, array $props = [])
    {
        return new WordLanguage($id, self::getProps($props));
    }

    private static function getProps(array $props)
    {
        return [
            WordLanguage::PROP_CODE => Parser::getArrayValue(self::PROP_CODE, $props),
            WordLanguage::PROP_NAME => Parser::getArrayValue(self::PROP_NAME, $props),
        ];
    }
}
