<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\BaseConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class ConditionGroupStub extends BaseConditionGroup
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'condition group';
    }
}
