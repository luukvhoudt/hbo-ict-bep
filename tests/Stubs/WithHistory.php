<?php

namespace LSVH\Lingo\Tests\Stubs;

trait WithHistory
{
    protected $histories = [];

    public function getHistoryByFuncName(string $funcName): ?History
    {
        $filteredHistories = array_filter($this->histories, function (History $history) use ($funcName) {
            return $history->getFuncName() === $funcName;
        });

        return empty($filteredHistories) ? null : current($filteredHistories);
    }

    public function addHistory(string $funcName, array $params = []): void
    {
        $history = $this->getHistoryByFuncName($funcName);
        if (empty($history)) {
            $history = new History($funcName);
        }
        $history->addRecord($params);

        $this->histories = array_merge(array_filter($this->histories, function (History $history) use ($funcName) {
            return $history->getFuncName() !== $funcName;
        }), [$history]);
    }
}
