<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\Structures;

abstract class BaseStructure implements Structure
{
    protected $name;

    protected $definition;

    public function __construct(string $name, string $definition)
    {
        $this->name = $name;
        $this->definition = $definition;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDefinition(): string
    {
        return $this->definition;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($name) use ($instances) {
            return new static($name, $instances[$name]);
        }, array_keys($instances));
    }
}
