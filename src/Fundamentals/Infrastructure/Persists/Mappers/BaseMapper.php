<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Mappers;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseIdentity;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Model;
use LSVH\Lingo\Utilities\Parser;

abstract class BaseMapper implements Mapper
{
    const PROP_ID = BaseIdentity::PROP_ID;

    public function toDataTransferObject(Model $model): array
    {
        $attributes = $model->getAttributes();

        return array_merge($attributes, $this->mapRelationToDataTransferObject($attributes));
    }

    public function toDataTransferObjects(array $models): array
    {
        return array_map(function (Model $model) {
            return $this->toDataTransferObject($model);
        }, $models);
    }

    public function fromDataTransferObject(Factory $factory, array $row): Model
    {
        return $factory::loadInstance($this->getIdentifierFromDataTransferObject($row), $row);
    }

    public function fromDataTransferObjects(Factory $factory, array $rows): array
    {
        return $factory::loadInstances($rows);
    }

    protected function mapRelationToDataTransferObject(array $attributes): array
    {
        $relations = array_filter($attributes, function ($attribute) {
            $arrayOfModels = is_array($attribute) && !empty(array_filter($attribute, function ($subAttribute) {
                return $subAttribute instanceof Model;
            }));

            return $attribute instanceof Model || $arrayOfModels;
        });

        return array_filter(array_map(function ($relation) {
            if (is_array($relation)) {
                return $this->toDataTransferObjects($relation);
            }

            return $this->toDataTransferObject($relation);
        }, $relations));
    }

    protected function getIdentifierFromDataTransferObject(array $row): int
    {
        return intval(Parser::getArrayValue(self::PROP_ID, $row));
    }
}
