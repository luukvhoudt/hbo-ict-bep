<?php

namespace LSVH\Lingo\Fundamentals\Application\Repositories;

use LSVH\Lingo\Components\Users\Domain\UserFactory;
use LSVH\Lingo\Fundamentals\Domain\Entities\BaseIdentity;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\ConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets\SQLConditionTarget;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues\SQLConditionValue;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\SQLCondition;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\SQLConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\SQLStructureGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\StructureGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\Structures\SQLTableStructure;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\SQLTable;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\Table;

abstract class BaseSQLRepository extends BaseRepository
{
    const CT_STRING = 'text';
    const CT_REQUIRED_STRING = self::CT_STRING.' not null';
    const CT_UNIQUE_STRING = self::CT_STRING.' not null unique';
    const CT_IDENTITY = 'integer generated always as identity primary key';
    const COLUMN_ID = BaseIdentity::PROP_ID;

    public static function getFactory(): Factory
    {
        return new UserFactory();
    }

    abstract protected static function getTableName(): string;

    protected static function getTableAlias(): ?string
    {
        return null;
    }

    public static function getTable(): Table
    {
        return new SQLTable(static::getTableName(), static::getTableAlias());
    }

    abstract protected static function getTableStructure(): array;

    protected static function getStructures(): array
    {
        $tableStructure = array_merge([self::COLUMN_ID => self::CT_IDENTITY], static::getTableStructure());

        return [
            new SQLTableStructure(static::getTable()->getName(), implode(',', $tableStructure)),
        ];
    }

    public static function getStructureGroup(): StructureGroup
    {
        return new SQLStructureGroup(static::getStructures());
    }

    public static function getConditionWhereIdEquals(int $value): ConditionGroup
    {
        return new SQLConditionGroup([
            new SQLCondition(new SQLConditionTarget(self::COLUMN_ID), new SQLConditionValue($value)),
        ]);
    }
}
