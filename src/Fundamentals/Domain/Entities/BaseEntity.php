<?php

namespace LSVH\Lingo\Fundamentals\Domain\Entities;

abstract class BaseEntity extends BaseIdentity implements Entity
{
    protected $props;

    public function __construct(int $id = null, array $props = [])
    {
        parent::__construct($id);
        $this->props = $props;
    }
}
