<?php

namespace LSVH\Lingo\Tests\Components\Turns\Domain;

use LSVH\Lingo\Components\Turns\Domain\TurnHint;
use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Utilities\Exceptions\RequiredArrayKeyException;
use LSVH\Lingo\Utilities\Exceptions\ValueHasLengthException;
use LSVH\Lingo\Utilities\Exceptions\ValueIsOneOfException;
use LSVH\Lingo\Utilities\Exceptions\ValueIsTypeOfException;

class TurnHintTest extends BaseTestCase
{
    /** @test */
    public function cannot_instantiate_without_type_prop()
    {
        $this->expectException(RequiredArrayKeyException::class);
        $this->expectExceptionMessageMatches('/`type`/');

        new TurnHint();
    }

    /** @test */
    public function cannot_instantiate_without_char_prop()
    {
        $props = [
            TurnHint::PROP_TYPE => TurnHint::ABSENT,
        ];

        $this->expectException(RequiredArrayKeyException::class);
        $this->expectExceptionMessageMatches('/`char`/');

        new TurnHint(null, $props);
    }

    /** @test */
    public function type_prop_should_be_string()
    {
        $props = [
            TurnHint::PROP_TYPE => intval(123),
        ];

        $this->expectException(ValueIsTypeOfException::class);

        new TurnHint(null, $props);
    }

    /** @test */
    public function char_prop_should_be_string()
    {
        $props = [
            TurnHint::PROP_TYPE => TurnHint::ABSENT,
            TurnHint::PROP_CHAR => intval(123),
        ];

        $this->expectException(ValueIsTypeOfException::class);

        new TurnHint(null, $props);
    }

    /** @test */
    public function type_prop_should_be_one_of_the_types()
    {
        $props = [
            TurnHint::PROP_TYPE => 'invalid',
            TurnHint::PROP_CHAR => 'c',
        ];

        $this->expectException(ValueIsOneOfException::class);

        new TurnHint(null, $props);
    }

    public function type_prop_data_provider(): array
    {
        return self::firstArrayItemAsKey([
            [TurnHint::ABSENT],
            [TurnHint::PRESENT],
            [TurnHint::CORRECT],
        ]);
    }

    /**
     * @test
     * @dataProvider type_prop_data_provider
     */
    public function type_prop_can_be_one_of_the_types($type)
    {
        $props = [
            TurnHint::PROP_TYPE => $type,
            TurnHint::PROP_CHAR => 'c',
        ];

        self::assertNotEmpty(new TurnHint(null, $props));
    }

    /** @test */
    public function char_prop_cannot_have_a_length_bigger_then_1()
    {
        $props = [
            TurnHint::PROP_TYPE => TurnHint::ABSENT,
            TurnHint::PROP_CHAR => 'char',
        ];

        $this->expectException(ValueHasLengthException::class);
        $this->expectExceptionMessageMatches('/`1`/');

        new TurnHint(null, $props);
    }
}
