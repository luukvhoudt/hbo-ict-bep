<?php

namespace LSVH\Lingo\Fundamentals\Application\Services;

use LSVH\Lingo\Fundamentals\Application\Repositories\Repository;

abstract class BaseService implements Service
{
    private $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function getRepository(): Repository
    {
        return $this->repository;
    }
}
