<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\BaseRelation;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class RelationStub extends BaseRelation
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'relation';
    }
}
