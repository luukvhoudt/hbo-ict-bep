<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups;

abstract class BaseStructureGroup implements StructureGroup
{
    private $structures;

    public function __construct(array $structures)
    {
        $this->structures = $structures;
    }

    public function getStructures(): array
    {
        return $this->structures;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($instance) {
            return new static($instance);
        }, $instances);
    }
}
