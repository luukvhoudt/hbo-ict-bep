<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Mappers;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Model;

interface Mapper
{
    public function toDataTransferObject(Model $model): array;

    public function toDataTransferObjects(array $models): array;

    public function fromDataTransferObject(Factory $factory, array $row): Model;

    public function fromDataTransferObjects(Factory $factory, array $rows): array;
}
