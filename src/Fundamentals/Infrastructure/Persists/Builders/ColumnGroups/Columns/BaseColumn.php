<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\Columns;

use LSVH\Lingo\Utilities\Parser;

abstract class BaseColumn implements Column
{
    const PROP_IDENTIFIER_ONLY = 'identifierOnly';

    private $identifierName;

    private $displayName;

    public function __construct(string $identifierName, string $displayName = null)
    {
        $this->identifierName = $identifierName;
        $this->displayName = $displayName;
    }

    public function getIdentifierName(): string
    {
        return $this->identifierName;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public static function createInstances(array $columns, array $defaults = []): array
    {
        $identifierOnly = boolval(Parser::getArrayValue(self::PROP_IDENTIFIER_ONLY, $defaults, false));

        return array_map(function ($identifierNameOrIndex) use ($columns, $identifierOnly) {
            $identifierOrDisplayName = $columns[$identifierNameOrIndex];

            if (!is_string($identifierNameOrIndex)) {
                return new static($identifierOrDisplayName);
            }

            if ($identifierOnly) {
                return new static($identifierNameOrIndex);
            }

            return new static($identifierNameOrIndex, $identifierOrDisplayName);
        }, array_keys($columns));
    }
}
