<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\BaseTable;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class TableStub extends BaseTable
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'table';
    }
}
