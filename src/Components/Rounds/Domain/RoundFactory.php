<?php

namespace LSVH\Lingo\Components\Rounds\Domain;

use LSVH\Lingo\Components\Turns\Domain\TurnFactory;
use LSVH\Lingo\Components\Words\Domain\WordFactory;
use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Fundamentals\Domain\Models\Round as RoundInterface;
use LSVH\Lingo\Fundamentals\Domain\Models\Word;
use LSVH\Lingo\Utilities\Parser;

class RoundFactory extends BaseFactory
{
    const PROP_WORD = Round::PROP_WORD;
    const PROP_TURNS = Round::PROP_TURNS;

    public static function createInstance(array $props = []): RoundInterface
    {
        return new Round(
            self::getIdentity(),
            self::getWord($props),
            self::getTurns($props)
        );
    }

    public static function loadInstance(int $id, array $props = []): RoundInterface
    {
        return new Round(
            self::getIdentity($id),
            self::getWord($props, true),
            self::getTurns($props, true)
        );
    }

    private static function getIdentity(int $id = null): RoundIdentity
    {
        return new RoundIdentity($id);
    }

    private static function getWord(array $props, bool $load = false): Word
    {
        $word = Parser::getArrayValue(self::PROP_WORD, $props, []);

        return self::createOrLoadInstanceWithFactory(WordFactory::class, $word, $load);
    }

    private static function getTurns(array $props, bool $load = false): array
    {
        $turns = Parser::getArrayValue(self::PROP_TURNS, $props, []);

        return self::createOrLoadInstancesWithFactory(TurnFactory::class, $turns, $load);
    }
}
