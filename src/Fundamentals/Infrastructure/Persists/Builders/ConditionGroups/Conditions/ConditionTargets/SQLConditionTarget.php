<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLConditionTarget extends BaseConditionTarget
{
    public function toFormattedString(Formatter $formatter): string
    {
        $formattedColumn = $formatter->formatIdentifier($this->getColumn());
        $unFormattedTable = $this->getTable();

        if (empty($unFormattedTable)) {
            return $formattedColumn;
        }

        return $formatter->formatIdentifier($unFormattedTable).'.'.$formattedColumn;
    }
}
