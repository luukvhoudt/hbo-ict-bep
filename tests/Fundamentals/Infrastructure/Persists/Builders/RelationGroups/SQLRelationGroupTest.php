<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\RelationGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\SQLRelationGroup;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\RelationStub;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\RelationTargetStub;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\TableStub;

class SQLRelationGroupTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $target = new RelationTargetStub(new TableStub('foo'), 'bar');
        $relation = new RelationStub($target, $target);
        $subject = new SQLRelationGroup([$relation, $relation]);

        $expected = $relation->toFormattedString($this->formatter);

        self::assertSame($expected.' '.$expected, $subject->toFormattedString($this->formatter));
    }

    /** @test */
    public function returns_empty_without_any_relations()
    {
        $subject = new SQLRelationGroup([]);

        self::assertEmpty($subject->toFormattedString($this->formatter));
    }
}
