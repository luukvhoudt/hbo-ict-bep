<?php

namespace LSVH\Lingo\Tests\Components\Games\Domain;

use LSVH\Lingo\Components\Games\Domain\Game;
use LSVH\Lingo\Components\Games\Domain\GameDifficulty;
use LSVH\Lingo\Components\Games\Domain\GameDifficultyLevel;
use LSVH\Lingo\Components\Games\Domain\GameIdentity;
use LSVH\Lingo\Components\Rounds\Domain\RoundFactory;
use LSVH\Lingo\Components\Words\Domain\WordFactory;
use LSVH\Lingo\Components\Words\Domain\WordLanguageFactory;
use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Tests\Stubs\Components\Domain\RoundStub;

class GameTest extends BaseTestCase
{
    private $game;

    private $gameId;

    private $gameDifficulty;

    private $roundProps;

    protected function setUp(): void
    {
        parent::setUp();
        $this->gameId = new GameIdentity();
        $level1 = new GameDifficultyLevel(null, [
            GameDifficultyLevel::PROP_WORD_LENGTH => 1,
        ]);
        $level2 = new GameDifficultyLevel(null, [
            GameDifficultyLevel::PROP_WORD_LENGTH => 2,
        ]);
        $this->gameDifficulty = new GameDifficulty(null, [
            GameDifficulty::PROP_NAME   => 'default',
            GameDifficulty::PROP_LEVELS => [$level1, $level2],
        ]);
        $this->game = new Game($this->gameId, $this->gameDifficulty);
        $this->roundProps = [
            RoundFactory::PROP_WORD => [
                WordFactory::PROP_VALUE    => 'hello',
                WordFactory::PROP_LANGUAGE => [
                    WordLanguageFactory::PROP_CODE => 'world',
                ],
            ],
        ];
    }

    /** @test */
    public function is_active_initially()
    {
        self::assertTrue($this->game->isActive());
    }

    /** @test */
    public function should_create_a_new_round()
    {
        self::assertEmpty($this->game->getActiveRound());

        $this->game->startOrContinueRound($this->roundProps);

        self::assertNotEmpty($this->game->getActiveRound());
    }

    /** @test */
    public function becomes_inactive_when_round_points_reaches_zero()
    {
        $this->game->startOrContinueRound($this->roundProps);

        $round = $this->game->getActiveRound();

        while ($round->calculatePoints() > 0) {
            $round->startOrContinueTurn();
            $round->endTurn('world');
        }

        self::assertTrue($this->game->wasActive());
    }

    /** @test */
    public function becomes_inactive_after_ending_a_round()
    {
        $this->game->startOrContinueRound($this->roundProps);

        $this->game->endRound();

        self::assertTrue($this->game->wasActive());
    }

    /** @test */
    public function remains_active_after_answering_a_turn_correctly()
    {
        $this->game->startOrContinueRound($this->roundProps);

        $round = $this->game->getActiveRound();
        $round->startOrContinueTurn();
        $round->endTurn('hello');

        self::assertTrue($this->game->isActive());
    }

    /** @test */
    public function continues_with_active_round_when_unanswered()
    {
        $this->game->startOrContinueRound($this->roundProps);
        $round = $this->game->getActiveRound();
        $round->startOrContinueTurn();
        $round->endTurn('world');

        // This round the points got decreased from 50 to 40.
        self::assertSame(40, $round->calculatePoints());

        $this->game->startOrContinueRound($this->roundProps);
        $round = $this->game->getActiveRound();

        // Same deal so same round
        self::assertSame(40, $round->calculatePoints());
    }

    /** @test */
    public function creates_a_new_round_when_answered_correctly()
    {
        $this->game->startOrContinueRound($this->roundProps);
        $round = $this->game->getActiveRound();
        $round->startOrContinueTurn();
        $round->endTurn('hello');

        self::assertSame(50, $round->calculatePoints());

        $this->game->startOrContinueRound($this->roundProps);
        $round = $this->game->getActiveRound();
        $round->startOrContinueTurn();
        $round->endTurn('world');

        self::assertSame(40, $round->calculatePoints());
    }

    /** @test */
    public function calculates_the_score_by_adding_up_all_correctly_answered_rounds_points()
    {
        $this->game->startOrContinueRound($this->roundProps);
        $round = $this->game->getActiveRound();
        $round->startOrContinueTurn();
        $round->endTurn('hello');

        $this->game->startOrContinueRound($this->roundProps);
        $round = $this->game->getActiveRound();
        $round->startOrContinueTurn();
        $round->endTurn('hello');

        self::assertSame(100, $this->game->calculateScore());
    }

    /** @test */
    public function the_level_changes_after_each_round()
    {
        $round = new RoundStub();
        $game = new Game($this->gameId, $this->gameDifficulty, [$round]);
        self::assertSame(1, $game->getActiveLevel()->getWordLength());

        $round = new RoundStub();
        $game = new Game($this->gameId, $this->gameDifficulty, [$round, $round]);
        self::assertSame(2, $game->getActiveLevel()->getWordLength());
    }

    /** @test */
    public function after_completing_all_levels_it_start_with_the_first_level_again()
    {
        $round = new RoundStub();
        $game = new Game($this->gameId, $this->gameDifficulty, [$round, $round, $round]);
        self::assertSame(1, $game->getActiveLevel()->getWordLength());
    }

    /** @test */
    public function calls_end_turn_until_it_reaches_zero()
    {
        $round = new RoundStub();
        $game = new Game($this->gameId, $this->gameDifficulty, [$round]);

        $game->endRound();

        self::assertCount(1, $round->getHistoryByFuncName('endTurn')->getRecords());
    }
}
