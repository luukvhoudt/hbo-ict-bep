<?php

namespace LSVH\Lingo\Components\Turns\Domain\Events;

use LSVH\Lingo\Fundamentals\Domain\Events\BaseEvent;

class ShouldCalculateHintsEvent extends BaseEvent
{
    protected $payload = true;
}
