<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets\BaseConditionTarget;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class ConditionTargetStub extends BaseConditionTarget
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'condition target';
    }
}
