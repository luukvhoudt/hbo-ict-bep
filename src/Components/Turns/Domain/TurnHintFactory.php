<?php

namespace LSVH\Lingo\Components\Turns\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Utilities\Parser;

class TurnHintFactory extends BaseFactory
{
    const PROP_TYPE = TurnHint::PROP_TYPE;
    const PROP_CHAR = TurnHint::PROP_CHAR;

    public static function createInstance(array $props = []): TurnHint
    {
        return new TurnHint(null, self::getProps($props));
    }

    public static function loadInstance(int $id, array $props = []): TurnHint
    {
        return new TurnHint($id, self::getProps($props));
    }

    private static function getProps(array $props): array
    {
        return [
            TurnHint::PROP_TYPE => Parser::getArrayValue(self::PROP_TYPE, $props),
            TurnHint::PROP_CHAR => Parser::getArrayValue(self::PROP_CHAR, $props),
        ];
    }
}
