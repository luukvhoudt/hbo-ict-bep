<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Builder;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Mappers\Mapper;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores\Store;

interface Persist
{
    public function __construct();

    public function getStore(): Store;

    public function getBuilder(): Builder;

    public function getMapper(): Mapper;
}
