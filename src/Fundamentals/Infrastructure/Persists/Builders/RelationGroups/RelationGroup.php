<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface RelationGroup extends BuilderEntity
{
    public function __construct(array $relations);

    public function getRelations(): array;
}
