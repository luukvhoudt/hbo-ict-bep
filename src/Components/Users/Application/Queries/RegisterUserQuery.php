<?php

namespace LSVH\Lingo\Components\Users\Application\Queries;

use LSVH\Lingo\Fundamentals\Application\Queries\BaseQuery;
use LSVH\Lingo\Fundamentals\Application\Services\Types\Create;

class RegisterUserQuery extends BaseQuery
{
    public function execute(array $query)
    {
        if (!in_array(Create::class, class_implements($this->service))) {
            return false;
        }

        return $this->service->create($query);
    }
}
