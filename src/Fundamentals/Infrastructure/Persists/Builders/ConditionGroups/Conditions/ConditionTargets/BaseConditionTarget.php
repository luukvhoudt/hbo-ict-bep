<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets;

abstract class BaseConditionTarget implements ConditionTarget
{
    private $column;

    private $table;

    public function __construct(string $column, string $table = null)
    {
        $this->column = $column;
        $this->table = $table;
    }

    public function getColumn(): string
    {
        return $this->column;
    }

    public function getTable(): ?string
    {
        return $this->table;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($columnOrIndex) use ($instances) {
            $columnOrTable = $instances[$columnOrIndex];

            if (is_string($columnOrIndex)) {
                return new static($columnOrIndex, $columnOrTable);
            }

            return new static($columnOrTable);
        }, array_keys($instances));
    }
}
