<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets\SQLConditionTarget;
use LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders\BuilderTestCase;

class SQLConditionTargetTest extends BuilderTestCase
{
    /** @test */
    public function can_correctly_format_to_string_for_store()
    {
        $subject = new SQLConditionTarget('foo');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`foo`', $actual);
    }

    /** @test */
    public function can_correctly_format_with_table_prefix_to_string_for_store()
    {
        $subject = new SQLConditionTarget('foo', 'bar');

        $actual = $subject->toFormattedString($this->formatter);

        self::assertSame('`bar`.`foo`', $actual);
    }
}
