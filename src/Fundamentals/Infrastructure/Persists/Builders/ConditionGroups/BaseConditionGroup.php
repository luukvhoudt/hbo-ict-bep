<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\Condition;

abstract class BaseConditionGroup implements ConditionGroup
{
    private $conditions;

    private $logicalOperator;

    public function __construct(array $conditions, string $logicalOperator = self::DEFAULT_LOGICAL_OPERATOR)
    {
        $this->conditions = $conditions;
        $this->logicalOperator = in_array(strtolower($logicalOperator), self::LOGICAL_OPERATORS)
            ? strtolower($logicalOperator) : self::DEFAULT_LOGICAL_OPERATOR;
    }

    /**
     * @return Condition[]|ConditionGroup[]
     */
    public function getConditions(): array
    {
        return $this->conditions;
    }

    public function getLogicalOperator(): string
    {
        return $this->logicalOperator;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($logicalOperatorOrIndex) use ($instances) {
            $condition = $instances[$logicalOperatorOrIndex];

            if (is_string($logicalOperatorOrIndex)) {
                return new static($condition, $logicalOperatorOrIndex);
            }

            return new static($condition);
        }, array_keys($instances));
    }
}
