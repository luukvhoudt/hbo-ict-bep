<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\Structures;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface Structure extends BuilderEntity
{
    public function __construct(string $name, string $definition);

    public function getName(): string;

    public function getDefinition(): string;
}
