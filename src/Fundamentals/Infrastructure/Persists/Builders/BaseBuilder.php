<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseIdentity;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ColumnGroups\ColumnGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\ConditionGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\RelationGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\StructureGroup;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\Tables\Table;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;
use LSVH\Lingo\Utilities\Checker;

abstract class BaseBuilder implements Builder
{
    const COLUMN_ID = BaseIdentity::PROP_ID;

    private $formatter;

    public function __construct(Formatter $formatter)
    {
        $this->formatter = $formatter;
    }

    public function setup(StructureGroup $structureGroup): string
    {
        return $structureGroup->toFormattedString($this->formatter);
    }

    public function insert(Table $table, array $data, array $columns): string
    {
        $tableName = $this->getFormattedTableName($table);
        $id = $this->formatIdentifier(static::COLUMN_ID);
        $formattedColumns = implode(',', array_map(function (string $column) {
            return $this->formatIdentifier($column);
        }, $columns));
        $rows = $this->getInsertRows($data);

        return "insert into $tableName ($formattedColumns) values $rows returning $id into id";
    }

    public function select(
        Table $table,
        ColumnGroup $columnGroup,
        RelationGroup $relationGroup = null,
        ConditionGroup $conditionGroup = null
    ): string {
        $tableName = $table->toFormattedString($this->formatter);
        $columns = $columnGroup->toFormattedString($this->formatter);
        $relations = empty($relationGroup) ? '' : ' '.$relationGroup->toFormattedString($this->formatter);
        $conditions = empty($conditionGroup) ? '' : ' where '.$conditionGroup->toFormattedString($this->formatter);

        return "select $columns from $tableName$relations$conditions";
    }

    public function update(Table $table, array $data, ConditionGroup $conditionGroup): string
    {
        $tableName = $this->getFormattedTableName($table);
        $sets = $this->getUpdateColumnsAndValues($data);
        $conditions = $conditionGroup->toFormattedString($this->formatter);

        return "update $tableName set $sets where $conditions";
    }

    public function delete(Table $table, ConditionGroup $conditionGroup): string
    {
        $tableName = $this->getFormattedTableName($table);
        $conditions = $conditionGroup->toFormattedString($this->formatter);

        return "delete from $tableName where $conditions";
    }

    private function formatIdentifier(string $name): string
    {
        return $this->formatter->formatIdentifier($name);
    }

    private function formatValue(string $value): string
    {
        return $this->formatter->formatValue($value);
    }

    private function getFormattedTableName(Table $table): string
    {
        return $this->formatIdentifier($table->getName());
    }

    private function getInsertRows(array $rows): string
    {
        if (Checker::isArrayOfArrays($rows)) {
            return $this->getInsertRow($rows);
        }

        return implode(',', array_map(function ($row) {
            return $this->getInsertRow($row);
        }, $rows));
    }

    private function getInsertRow(array $values): string
    {
        $valueString = implode(',', array_map(function ($value) {
            return $this->formatValue($value);
        }, $values));

        return '('.$valueString.')';
    }

    private function getUpdateColumnsAndValues(array $columnsAndValues): string
    {
        return implode(',', array_map(function ($column) use ($columnsAndValues) {
            $value = $columnsAndValues[$column];

            return $this->formatIdentifier($column).' = '.$this->formatValue($value);
        }, array_keys($columnsAndValues)));
    }
}
