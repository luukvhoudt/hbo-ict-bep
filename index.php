<?php

class FileNotFoundException extends Exception
{
}

$autoloader = 'vendor/autoload.php';

if (!file_exists($autoloader)) {
    throw new FileNotFoundException("File not found: $autoloader");
}

require_once $autoloader;
