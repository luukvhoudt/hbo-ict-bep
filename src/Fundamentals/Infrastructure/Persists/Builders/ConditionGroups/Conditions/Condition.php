<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionTargets\ConditionTarget;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\ConditionGroups\Conditions\ConditionValues\ConditionValue;

interface Condition extends BuilderEntity
{
    const COMPARISON_OPERATORS = ['=', '>', '=>', '<', '<=', '<>', 'between', 'in', 'like'];
    const DEFAULT_COMPARISON_OPERATOR = self::COMPARISON_OPERATORS[0];

    public function __construct(
        ConditionTarget $target,
        ConditionValue $value,
        $comparisonOperator = self::DEFAULT_COMPARISON_OPERATOR
    );

    public function getTarget(): ConditionTarget;

    public function getValue(): ConditionValue;

    public function getComparisonOperator(): ?string;
}
