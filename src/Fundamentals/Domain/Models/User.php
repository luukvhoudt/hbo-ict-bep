<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface User extends Model
{
    public function startOrContinueGame(array $props): void;

    public function getActiveGame(): ?Game;
}
