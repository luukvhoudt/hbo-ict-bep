<?php

namespace LSVH\Lingo\Components\Games\Domain;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseEntity;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Level as LevelInterface;
use LSVH\Lingo\Utilities\Guard;
use LSVH\Lingo\Utilities\Parser;

class GameDifficultyLevel extends BaseEntity implements LevelInterface
{
    const PROP_ORDER = 'order';
    const PROP_WORD_LENGTH = 'word_length';

    public function __construct(int $id = null, array $props = [])
    {
        parent::__construct($id, $props);
        Guard::toRequireArrayKey(self::PROP_WORD_LENGTH, $props);
        Guard::valueToBeTypeOf($props[self::PROP_WORD_LENGTH], 'integer');
    }

    public static function getFactory(): Factory
    {
        return new GameDifficultyLevelFactory();
    }

    public static function getShortName(): string
    {
        return 'gdl';
    }

    public static function getPluralName(): string
    {
        return 'game difficulty levels';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID          => $this->getIdentity(),
            self::PROP_ORDER       => $this->getOrder(),
            self::PROP_WORD_LENGTH => $this->getWordLength(),
        ];
    }

    public function getOrder(): int
    {
        return Parser::getArrayValue(self::PROP_ORDER, $this->props, 0);
    }

    public function getWordLength(): int
    {
        return Parser::getArrayValue(self::PROP_WORD_LENGTH, $this->props, 1);
    }
}
