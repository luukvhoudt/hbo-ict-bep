<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\BuilderEntity;

interface StructureGroup extends BuilderEntity
{
    public function __construct(array $structures);

    public function getStructures(): array;
}
