<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Stores;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores\LaminasStore;
use LSVH\Lingo\Tests\BaseTestCase;

class LaminasStoreTest extends BaseTestCase
{
    private $store;

    protected function setUp(): void
    {
        parent::setUp();
        $this->store = new LaminasStore();
    }

    /** @test */
    public function can_query_and_read_result_of_single_row()
    {
        $expected = 'hello world';
        $stmt = "select '$expected' as c";
        $result = $this->store->query($stmt);

        $rows = $result->getRows();
        self::assertSame($expected, $rows[0]['c']);
    }

    /** @test */
    public function can_query_and_read_results_of_multiple_rows()
    {
        $stmt = 'select * from (values (1, 2), (3, 4)) as q (c1, c2)';
        $result = $this->store->query($stmt);

        $rows = $result->getRows();
        self::assertSame(1, $rows[0]['c1']);
        self::assertSame(2, $rows[0]['c2']);
        self::assertSame(3, $rows[1]['c1']);
        self::assertSame(4, $rows[1]['c2']);
    }
}
