<?php

namespace LSVH\Lingo\Fundamentals\Application\Queries;

use LSVH\Lingo\Fundamentals\Application\Services\Service;

interface Query
{
    public function __construct(Service $service);

    public function execute(array $query);
}
