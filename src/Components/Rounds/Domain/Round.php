<?php

namespace LSVH\Lingo\Components\Rounds\Domain;

use LSVH\Lingo\Components\Turns\Domain\TurnFactory;
use LSVH\Lingo\Fundamentals\Domain\Aggregates\BaseAggregate;
use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Round as RoundInterface;
use LSVH\Lingo\Fundamentals\Domain\Models\Turn;
use LSVH\Lingo\Fundamentals\Domain\Models\Word;
use LSVH\Lingo\Utilities\EnvVars;

class Round extends BaseAggregate implements RoundInterface
{
    const PROP_ID = RoundIdentity::PROP_ID;
    const PROP_WORD = 'word';
    const PROP_TURNS = 'turns';

    /**
     * @var RoundIdentity
     */
    private $id;

    /**
     * @var Word
     */
    private $word;

    /**
     * @var Turn[]
     */
    private $turns;

    public function __construct(RoundIdentity $id, Word $word, array $turns = [])
    {
        $this->id = $id;
        $this->word = $word;
        $this->turns = $turns;
    }

    public function getIdentity(): ?int
    {
        return $this->id->getIdentity();
    }

    public static function getFactory(): Factory
    {
        return new RoundFactory();
    }

    public static function getShortName(): string
    {
        return 'r';
    }

    public static function getPluralName(): string
    {
        return 'rounds';
    }

    public function getAttributes(): array
    {
        return [
            self::PROP_ID    => $this->id->getIdentity(),
            self::PROP_WORD  => $this->word,
            self::PROP_TURNS => $this->turns,
        ];
    }

    public function isActive(): bool
    {
        if (empty($this->turns)) {
            return true;
        }

        $turn = $this->getActiveTurn();
        if (empty($turn) && $this->calculatePoints() <= 0) {
            return false;
        }

        return !$this->gaveCorrectAnswer();
    }

    public function wasActive(): bool
    {
        return !$this->isActive();
    }

    public function getActiveTurn(): ?Turn
    {
        $activeTurns = array_filter($this->turns, function (Turn $turn) {
            return $turn->isActive();
        });

        return empty($activeTurns) ? null : current($activeTurns);
    }

    public function startOrContinueTurn(): void
    {
        if (empty($this->getActiveTurn()) && $this->calculatePoints() >= 0) {
            $this->turns[] = TurnFactory::createInstance();
        }
    }

    public function endTurn(string $answer): void
    {
        $turn = $this->getActiveTurn();
        if (!empty($turn)) {
            $turn->submitAnswer($answer);
            $turn->validateAnswer($this->word->getValue());
        }
    }

    public function calculatePoints(): int
    {
        $failedTurns = array_filter($this->turns, function (Turn $turn) {
            return !$turn->isAnsweredCorrectly();
        });

        $maxPointsPerRound = EnvVars::getMaxPointsPerRound();
        $pointsSubtractedPerTurn = EnvVars::getPointsSubtractedPerTurn();

        return $maxPointsPerRound - (count($failedTurns) * $pointsSubtractedPerTurn);
    }

    public function gaveCorrectAnswer(): bool
    {
        return !empty(array_filter($this->turns, function (Turn $turn) {
            return $turn->isAnsweredCorrectly();
        }));
    }
}
