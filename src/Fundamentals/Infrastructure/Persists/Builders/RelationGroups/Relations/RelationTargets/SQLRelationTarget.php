<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLRelationTarget extends BaseRelationTarget
{
    public function toFormattedString(Formatter $formatter): string
    {
        $tableAlias = $this->getTable()->getAlias();
        $tableName = $formatter->formatIdentifier($this->getTable()->getName());
        $formattedColumn = $formatter->formatIdentifier($this->getColumn());
        $suffix = '.'.$formattedColumn;

        if (!empty($tableAlias)) {
            return $formatter->formatIdentifier($tableAlias).$suffix;
        }

        return $tableName.$suffix;
    }
}
