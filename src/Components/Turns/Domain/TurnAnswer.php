<?php

namespace LSVH\Lingo\Components\Turns\Domain;

class TurnAnswer
{
    /**
     * @var string|null
     */
    private $value;

    public function __construct(string $value = null)
    {
        $this->value = $value;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): void
    {
        $this->value = $value;
    }
}
