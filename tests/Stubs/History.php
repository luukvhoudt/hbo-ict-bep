<?php

namespace LSVH\Lingo\Tests\Stubs;

class History
{
    private $funcName;

    private $records = [];

    public function __construct(string $funcName)
    {
        $this->funcName = $funcName;
    }

    public function addRecord(array $params): void
    {
        $this->records[] = $params;
    }

    public function getFuncName(): string
    {
        return $this->funcName;
    }

    public function getRecords(): array
    {
        return $this->records;
    }
}
