<?php

namespace LSVH\Lingo\Components\Words\Exceptions;

use Exception;
use Throwable;

class InvalidWordFormatException extends Exception
{
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = "The format of the word `$message` is invalid.";
        parent::__construct($message, $code, $previous);
    }
}
