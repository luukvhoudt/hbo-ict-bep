<?php

namespace LSVH\Lingo\Utilities\Exceptions;

use Exception;

class ValueIsOneOfException extends Exception
{
    const PROP_VALUE = 'value';
    const PROP_ENUM = 'enum';

    public function __construct($message, $code = 0, $previous = null)
    {
        $value = array_key_exists(self::PROP_VALUE, $message) ? $message[self::PROP_VALUE] : '';
        $expected = array_key_exists(self::PROP_ENUM, $message) ? $message[self::PROP_ENUM] : [];
        $expected = implode(', ', $expected);
        $message = "Incorrect value given, expected `$value` to be in present in enum `$expected`.";
        parent::__construct($message, $code, $previous);
    }
}
