<?php

namespace LSVH\Lingo\Utilities;

use LSVH\Lingo\Utilities\Exceptions\RequiredArrayKeyException;
use LSVH\Lingo\Utilities\Exceptions\ValueHasLengthException;
use LSVH\Lingo\Utilities\Exceptions\ValueIsEmptyException;
use LSVH\Lingo\Utilities\Exceptions\ValueIsOneOfException;
use LSVH\Lingo\Utilities\Exceptions\ValueIsTypeOfException;

abstract class Guard
{
    const REQUIRED_PROP_KEY = RequiredArrayKeyException::PROP_KEY;
    const REQUIRED_PROP_ARRAY = RequiredArrayKeyException::PROP_ARRAY;
    const TYPE_OF_PROP_VALUE = ValueIsTypeOfException::PROP_VALUE;
    const TYPE_OF_PROP_TYPE = ValueIsTypeOfException::PROP_TYPE;
    const ONE_OF_PROP_VALUE = ValueIsOneOfException::PROP_VALUE;
    const ONE_OF_PROP_ENUM = ValueIsOneOfException::PROP_ENUM;
    const HAS_LENGTH_PROP_VALUE = ValueHasLengthException::PROP_VALUE;
    const HAS_LENGTH_PROP_LENGTH = ValueHasLengthException::PROP_LENGTH;

    public static function toRequireArrayKey(string $key, array $array): void
    {
        if (!array_key_exists($key, $array)) {
            throw new RequiredArrayKeyException([
                self::REQUIRED_PROP_KEY   => $key,
                self::REQUIRED_PROP_ARRAY => $array,
            ]);
        }
    }

    public static function valueToBeTypeOf($value, $type)
    {
        if (!(gettype($value) === $type)) {
            throw new ValueIsTypeOfException([
                self::TYPE_OF_PROP_VALUE => $value,
                self::TYPE_OF_PROP_TYPE  => $type,
            ]);
        }
    }

    public static function valueToBeOneOf($value, array $enum): void
    {
        if (!in_array($value, $enum)) {
            throw new ValueIsOneOfException([
                self::ONE_OF_PROP_VALUE => $value,
                self::ONE_OF_PROP_ENUM  => $enum,
            ]);
        }
    }

    public static function stringValueToHaveLength(string $value, int $length): void
    {
        if (strlen($value) != $length) {
            throw new ValueHasLengthException([
                self::HAS_LENGTH_PROP_VALUE  => $value,
                self::HAS_LENGTH_PROP_LENGTH => $length,
            ]);
        }
    }

    public static function valueToNotBeEmpty($value): void
    {
        if (empty($value)) {
            throw new ValueIsEmptyException($value);
        }
    }
}
