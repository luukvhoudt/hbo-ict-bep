<?php

namespace LSVH\Lingo\Components\Games\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\BaseFactory;
use LSVH\Lingo\Utilities\Parser;

class GameDifficultyFactory extends BaseFactory
{
    const PROP_NAME = GameDifficulty::PROP_NAME;
    const PROP_LEVELS = GameDifficulty::PROP_LEVELS;

    public static function createInstance(array $props = []): GameDifficulty
    {
        return new GameDifficulty(null, self::getProps($props));
    }

    public static function loadInstance(int $id, array $props = []): GameDifficulty
    {
        return new GameDifficulty($id, self::getProps($props, true));
    }

    private static function getProps(array $props, bool $load = false): array
    {
        return [
            GameDifficulty::PROP_NAME   => Parser::getArrayValue(self::PROP_NAME, $props),
            GameDifficulty::PROP_LEVELS => self::getLevels($props, $load),
        ];
    }

    private static function getLevels(array $props, bool $load = false): array
    {
        $rounds = Parser::getArrayValue(self::PROP_LEVELS, $props, []);

        return self::createOrLoadInstancesWithFactory(GameDifficultyLevelFactory::class, $rounds, $load);
    }
}
