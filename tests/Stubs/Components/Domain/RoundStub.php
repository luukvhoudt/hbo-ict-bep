<?php

namespace LSVH\Lingo\Tests\Stubs\Components\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Round;
use LSVH\Lingo\Fundamentals\Domain\Models\Turn;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Domain\FactoryStub;
use LSVH\Lingo\Tests\Stubs\WithHistory;

class RoundStub implements Round
{
    use WithHistory;

    private $points = 10;

    public function getIdentity(): ?int
    {
        return null;
    }

    public static function getFactory(): Factory
    {
        return new FactoryStub();
    }

    public static function getShortName(): string
    {
        return '';
    }

    public static function getPluralName(): string
    {
        return '';
    }

    public function isActive(): bool
    {
        return true;
    }

    public function wasActive(): bool
    {
        return false;
    }

    public function startOrContinueTurn(): void
    {
        $this->addHistory('startOrContinueTurn');
    }

    public function endTurn(string $answer): void
    {
        $this->points = 0;
        $this->addHistory('endTurn', [$answer]);
    }

    public function getActiveTurn(): ?Turn
    {
        return new TurnStub();
    }

    public function calculatePoints(): int
    {
        return $this->points;
    }

    public function gaveCorrectAnswer(): bool
    {
        return true;
    }

    public function getAttributes(): array
    {
        return [];
    }
}
