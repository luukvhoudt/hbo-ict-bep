<?php

namespace LSVH\Lingo\Utilities;

abstract class EnvVars
{
    public static function getLanguageCodeDefault(): ?string
    {
        return self::env('APP_LANGUAGE_CODE_DEFAULT');
    }

    public static function getWordFormatRegex(): string
    {
        return self::env('APP_WORD_FORMAT', '');
    }

    public static function getWordTrimRegex(): string
    {
        return self::env('APP_WORD_TRIM', '');
    }

    public static function getMaxPointsPerRound(): int
    {
        return intval(self::env('APP_MAX_POINTS_PER_ROUND', 0));
    }

    public static function getPointsSubtractedPerTurn(): int
    {
        return intval(self::env('APP_POINTS_SUBTRACTED_PER_TURN', 0));
    }

    public static function getTurnDuration(): int
    {
        return intval(self::env('APP_TURN_DURATION', 0));
    }

    public static function getDatabaseDriver(): string
    {
        return self::env('DB_DRIVER', 'pdo');
    }

    public static function getDatabasePlatform(): string
    {
        return self::env('DB_PLATFORM', 'pgsql');
    }

    public static function getDatabaseHost(): string
    {
        return self::env('DB_HOST', 'postgres');
    }

    public static function getDatabasePort(): string
    {
        return self::env('DB_PORT', 5432);
    }

    public static function getDatabaseName(): string
    {
        $env = self::env('APP_ENV', 'production');

        return self::env('DB_NAME', 'lingo_db').'_'.$env;
    }

    public static function getDatabaseUsername(): string
    {
        return self::env('DB_USER', 'lingo_user');
    }

    public static function getDatabasePassword(): string
    {
        return self::env('DB_PASS', 'secret');
    }

    private static function env(string $key, $fallback = null)
    {
        $var = getenv($key);

        return !empty($var) ? $var : $fallback;
    }
}
