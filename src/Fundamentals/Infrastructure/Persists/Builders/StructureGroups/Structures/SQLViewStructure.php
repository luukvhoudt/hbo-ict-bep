<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\StructureGroups\Structures;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class SQLViewStructure extends BaseStructure
{
    public function toFormattedString(Formatter $formatter): string
    {
        $tableName = $formatter->formatIdentifier($this->getName());
        $definition = $this->getDefinition();

        return "create or replace view $tableName AS $definition";
    }
}
