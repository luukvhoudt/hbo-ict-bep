<?php

namespace LSVH\Lingo\Utilities;

abstract class Checker
{
    public static function isArrayOfArrays(array $value): bool
    {
        return empty(array_filter($value, 'is_array'));
    }
}
