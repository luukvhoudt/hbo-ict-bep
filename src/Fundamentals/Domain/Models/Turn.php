<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface Turn extends Model, Activatable
{
    public function isAnsweredCorrectly(): bool;

    public function submitAnswer(string $guessedAnswer): void;

    public function validateAnswer(string $correctAnswer): void;

    public function getHints(): array;
}
