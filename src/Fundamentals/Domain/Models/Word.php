<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface Word extends Model
{
    public function getValue(): string;

    public function getLength(): int;

    public function getLanguageCode(): string;

    public static function trim(string $value): string;

    public static function isValidFormat(string $value): bool;
}
