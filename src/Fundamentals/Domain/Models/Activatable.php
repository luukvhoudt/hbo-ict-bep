<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

interface Activatable
{
    public function isActive(): bool;

    public function wasActive(): bool;
}
