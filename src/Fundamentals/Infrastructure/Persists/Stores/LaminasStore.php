<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores;

use Laminas\Db\Adapter\Adapter;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results\LaminasResult;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results\Result;
use LSVH\Lingo\Utilities\EnvVars;

class LaminasStore implements Store
{
    private $adapter;

    public function __construct()
    {
        $platform = EnvVars::getDatabasePlatform();
        $dbHost = EnvVars::getDatabaseHost();
        $dbPort = EnvVars::getDatabasePort();
        $dbName = EnvVars::getDatabaseName();
        $dbUser = EnvVars::getDatabaseUsername();
        $dbPass = EnvVars::getDatabasePassword();
        $dsn = '%s:host=%s;port=%d;dbname=%s;user=%s;password=%s';

        $this->adapter = new Adapter([
            'driver'   => EnvVars::getDatabaseDriver(),
            'platform' => $platform,
            'dsn'      => sprintf($dsn, $platform, $dbHost, $dbPort, $dbName, $dbUser, $dbPass),
        ]);
    }

    public function query(string $statement): Result
    {
        return new LaminasResult($this->adapter->query($statement, Adapter::QUERY_MODE_EXECUTE));
    }
}
