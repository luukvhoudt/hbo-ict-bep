<?php

namespace LSVH\Lingo\Fundamentals\Domain\Aggregates;

use LSVH\Lingo\Fundamentals\Domain\Events\Event;
use LSVH\Lingo\Fundamentals\Domain\Subscribers\Subscriber;

abstract class BaseAggregate implements Aggregate
{
    protected $subscribers = [];

    public function subscribe(Subscriber $subscriber): void
    {
        $this->subscribers[] = $subscriber;
    }

    public function unsubscribe(Subscriber $subscriber): void
    {
        $this->subscribers = array_filter(
            $this->subscribers,
            function (Subscriber $anotherSubscriber) use ($subscriber) {
                return $subscriber->getEvent()->isEqualTo($anotherSubscriber->getEvent());
            }
        );
    }

    public function notify(Event $event): void
    {
        array_walk($this->subscribers, function (Subscriber $subscriber) use ($event) {
            if ($subscriber->isSubscribedTo($event)) {
                $subscriber->executeHandler($event);
            }
        });
    }
}
