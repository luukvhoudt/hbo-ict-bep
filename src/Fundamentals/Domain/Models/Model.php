<?php

namespace LSVH\Lingo\Fundamentals\Domain\Models;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;

interface Model
{
    public function getIdentity(): ?int;

    public static function getShortName(): string;

    public static function getPluralName(): string;

    public static function getFactory(): Factory;

    public function getAttributes(): array;
}
