<?php

namespace LSVH\Lingo\Tests\Components\Users\Domain;

use LSVH\Lingo\Components\Games\Domain\GameDifficultyFactory;
use LSVH\Lingo\Components\Games\Domain\GameDifficultyLevelFactory;
use LSVH\Lingo\Components\Games\Domain\GameFactory;
use LSVH\Lingo\Components\Rounds\Domain\RoundFactory;
use LSVH\Lingo\Components\Users\Domain\User;
use LSVH\Lingo\Components\Users\Domain\UserIdentity;
use LSVH\Lingo\Components\Users\Domain\UserLogin;
use LSVH\Lingo\Components\Words\Domain\WordFactory;
use LSVH\Lingo\Components\Words\Domain\WordLanguageFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class UserTest extends BaseTestCase
{
    private $user;

    private $gameProps;

    private $roundProps;

    protected function setUp(): void
    {
        parent::setUp();

        $id = new UserIdentity();
        $login = new UserLogin('hello', 'world');

        $this->user = new User($id, $login);

        $this->gameProps = [
            GameFactory::PROP_DIFFICULTY => [
                GameDifficultyFactory::PROP_NAME   => 'hello',
                GameDifficultyFactory::PROP_LEVELS => [
                    [
                        GameDifficultyLevelFactory::PROP_ORDER       => 1,
                        GameDifficultyLevelFactory::PROP_WORD_LENGTH => 2,
                    ],
                ],
            ],
        ];

        $this->roundProps = [
            RoundFactory::PROP_WORD => [
                WordFactory::PROP_VALUE    => 'hello',
                WordFactory::PROP_LANGUAGE => [
                    WordLanguageFactory::PROP_CODE => 'world',
                ],
            ],
        ];
    }

    /** @test */
    public function starts_a_new_game_when_none_is_active()
    {
        self::assertEmpty($this->user->getActiveGame());

        $this->user->startOrContinueGame($this->gameProps);

        self::assertNotEmpty($this->user->getActiveGame());
    }

    /** @test */
    public function after_ending_a_game_there_are_no_active_games()
    {
        $this->user->startOrContinueGame($this->gameProps);

        $game = $this->user->getActiveGame();
        self::assertNotEmpty($game);

        $game->startOrContinueRound($this->roundProps);
        $this->user->endGame();

        self::assertEmpty($this->user->getActiveGame());
    }

    /** @test */
    public function cannot_end_game_when_no_round_has_started_yet()
    {
        $this->user->startOrContinueGame($this->gameProps);

        self::assertNotEmpty($this->user->getActiveGame());

        $this->user->endGame();

        self::assertNotEmpty($this->user->getActiveGame());
    }
}
