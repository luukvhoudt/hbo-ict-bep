<?php

namespace LSVH\Lingo\Fundamentals\Domain\Subscribers;

use LSVH\Lingo\Fundamentals\Domain\Events\Event;

interface Subscriber
{
    public function __construct(Event $event, callable $handler);

    public function getEvent(): Event;

    public function isSubscribedTo(Event $event): bool;

    public function executeHandler(Event $event): void;
}
