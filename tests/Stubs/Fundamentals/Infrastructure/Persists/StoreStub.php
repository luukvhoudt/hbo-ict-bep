<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Results\Result;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Stores\Store;
use LSVH\Lingo\Tests\Stubs\WithHistory;

class StoreStub implements Store
{
    use WithHistory;

    public function query(string $statement): Result
    {
        $this->addHistory('query', [$statement]);

        return new ResultStub();
    }

    public function formatIdentifier(string $value): string
    {
        return '`'.$value.'`';
    }

    public function formatValue(string $value): string
    {
        return "'".$value."'";
    }
}
