<?php

namespace LSVH\Lingo\Tests\Components\Users\Domain;

use LSVH\Lingo\Components\Users\Domain\UserFactory;
use LSVH\Lingo\Tests\BaseTestCase;

class UserFactoryTest extends BaseTestCase
{
    private $createProps;

    private $loadProps;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createProps = [
            UserFactory::PROP_USER => 'foobar',
            UserFactory::PROP_PASS => 'secret123',
        ];
        $this->loadProps = array_merge([
            UserFactory::PROP_ID => 1,
        ], $this->createProps);
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = UserFactory::createInstance($this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = UserFactory::loadInstance(1, $this->loadProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            $this->createProps, $this->createProps,
        ];

        $actual = UserFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            $this->loadProps, $this->loadProps,
        ];

        $actual = UserFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }
}
