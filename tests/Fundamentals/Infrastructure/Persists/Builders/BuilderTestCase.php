<?php

namespace LSVH\Lingo\Tests\Fundamentals\Infrastructure\Persists\Builders;

use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists\FormatterStub;

abstract class BuilderTestCase extends BaseTestCase
{
    protected $formatter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->formatter = new FormatterStub();
    }
}
