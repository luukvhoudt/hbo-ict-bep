<?php

namespace LSVH\Lingo\Fundamentals\Domain\Factories;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseIdentity;
use LSVH\Lingo\Utilities\Parser;

abstract class BaseFactory implements Factory
{
    const PROP_ID = BaseIdentity::PROP_ID;

    public static function createInstances(array $values = [], array $defaultValue = []): array
    {
        return array_map(function ($value) use ($defaultValue) {
            $value = is_array($value) ? $value : [];

            return static::createInstance(array_merge($defaultValue, $value));
        }, $values);
    }

    public static function loadInstances(array $values, array $defaultValue = []): array
    {
        return array_map(function ($value) use ($defaultValue) {
            $value = is_array($value) ? $value : [];
            $id = intval(Parser::getArrayValue(static::PROP_ID, $value));

            return static::loadInstance($id, array_merge($defaultValue, $value));
        }, $values);
    }

    protected static function createOrLoadInstanceWithFactory(string $factory, array $props, bool $load = false)
    {
        if ($load) {
            $id = intval(Parser::getArrayValue(self::PROP_ID, $props));

            return call_user_func([$factory, 'loadInstance'], $id, $props);
        }

        return call_user_func([$factory, 'createInstance'], $props);
    }

    protected static function createOrLoadInstancesWithFactory(
        string $factory,
        array $values,
        bool $load = false
    ): array {
        if (empty($values)) {
            return [];
        }

        if ($load) {
            return call_user_func([$factory, 'loadInstances'], $values);
        }

        return call_user_func([$factory, 'createInstances'], $values);
    }
}
