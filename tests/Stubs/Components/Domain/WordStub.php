<?php

namespace LSVH\Lingo\Tests\Stubs\Components\Domain;

use LSVH\Lingo\Fundamentals\Domain\Factories\Factory;
use LSVH\Lingo\Fundamentals\Domain\Models\Word;
use LSVH\Lingo\Tests\Stubs\Fundamentals\Domain\FactoryStub;

class WordStub implements Word
{
    const VALUE = 'hello';
    const LANG_CODE = 'world';

    public function getIdentity(): ?int
    {
        return null;
    }

    public static function getFactory(): Factory
    {
        return new FactoryStub();
    }

    public static function getShortName(): string
    {
        return '';
    }

    public static function getPluralName(): string
    {
        return '';
    }

    public function getValue(): string
    {
        return self::VALUE;
    }

    public function getLength(): int
    {
        return 5;
    }

    public function getLanguageCode(): string
    {
        return self::LANG_CODE;
    }

    public static function trim(string $value): string
    {
        return $value;
    }

    public static function isValidFormat(string $value): bool
    {
        return true;
    }

    public function getAttributes(): array
    {
        return [];
    }
}
