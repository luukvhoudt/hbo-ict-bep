<?php

namespace LSVH\Lingo\Fundamentals\Domain\Events;

use LSVH\Lingo\Fundamentals\Domain\Entities\BaseIdentity;

abstract class BaseEvent extends BaseIdentity implements Event
{
    protected $args;

    protected $payload;

    protected $timestamp;

    public function __construct(int $value = null, array $args = [])
    {
        parent::__construct($value);
        $this->args = $args;
        $this->timestamp = time();
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function isEqualTo(Event $anotherEvent)
    {
        return $this instanceof $anotherEvent && $this->getIdentity() === $anotherEvent->getIdentity();
    }

    public function getArguments(): array
    {
        return $this->args;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function setPayload($value): void
    {
        $this->payload = $value;
    }
}
