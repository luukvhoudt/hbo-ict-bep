<?php

namespace LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups;

abstract class BaseRelationGroup implements RelationGroup
{
    private $relations;

    public function __construct(array $relations)
    {
        $this->relations = $relations;
    }

    public function getRelations(): array
    {
        return $this->relations;
    }

    public static function createInstances(array $instances, array $defaults = []): array
    {
        return array_map(function ($instance) {
            return new static($instance);
        }, $instances);
    }
}
