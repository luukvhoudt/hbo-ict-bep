<?php

namespace LSVH\Lingo\Fundamentals\Domain\Events;

use LSVH\Lingo\Fundamentals\Domain\Entities\Identity;

interface Event extends Identity
{
    public function getTimestamp(): int;

    public function isEqualTo(self $anotherEvent);

    public function getArguments(): array;

    public function setPayload($value): void;

    public function getPayload();
}
