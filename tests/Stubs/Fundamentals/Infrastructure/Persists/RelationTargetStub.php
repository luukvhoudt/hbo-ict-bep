<?php

namespace LSVH\Lingo\Tests\Stubs\Fundamentals\Infrastructure\Persists;

use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Builders\RelationGroups\Relations\RelationTargets\BaseRelationTarget;
use LSVH\Lingo\Fundamentals\Infrastructure\Persists\Formatters\Formatter;

class RelationTargetStub extends BaseRelationTarget
{
    public function toFormattedString(Formatter $formatter): string
    {
        return 'relation target';
    }
}
