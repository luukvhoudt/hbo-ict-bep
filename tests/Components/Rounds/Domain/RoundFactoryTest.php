<?php

namespace LSVH\Lingo\Tests\Components\Rounds\Domain;

use LSVH\Lingo\Components\Rounds\Domain\RoundFactory;
use LSVH\Lingo\Components\Words\Domain\WordFactory;
use LSVH\Lingo\Components\Words\Domain\WordLanguageFactory;
use LSVH\Lingo\Tests\BaseTestCase;
use LSVH\Lingo\Tests\Stubs\Components\Domain\WordStub;

class RoundFactoryTest extends BaseTestCase
{
    private $createProps;

    private $loadProps;

    protected function setUp(): void
    {
        parent::setUp();

        $word = new WordStub();
        $this->createProps = [
            RoundFactory::PROP_WORD => [
                WordFactory::PROP_VALUE    => $word->getValue(),
                WordFactory::PROP_LANGUAGE => [
                    WordLanguageFactory::PROP_CODE => $word->getLanguageCode(),
                ],
            ],
        ];
        $this->loadProps = [
            RoundFactory::PROP_ID   => 1,
            RoundFactory::PROP_WORD => [
                WordFactory::PROP_ID       => 1,
                WordFactory::PROP_VALUE    => $word->getValue(),
                WordFactory::PROP_LANGUAGE => [
                    WordLanguageFactory::PROP_ID   => 1,
                    WordLanguageFactory::PROP_CODE => $word->getLanguageCode(),
                ],
            ],
        ];
    }

    /** @test */
    public function can_create_a_single_instance()
    {
        $actual = RoundFactory::createInstance($this->createProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_load_a_single_instance()
    {
        $actual = RoundFactory::loadInstance(1, $this->loadProps);

        self::assertNotEmpty($actual);
    }

    /** @test */
    public function can_create_multiple_instance()
    {
        $values = [
            $this->createProps, $this->createProps,
        ];

        $actual = RoundFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }

    /** @test */
    public function can_load_multiple_instances()
    {
        $values = [
            $this->loadProps, $this->loadProps,
        ];

        $actual = RoundFactory::createInstances($values);

        self::assertNotEmpty($actual);
        self::assertCount(2, $actual);
    }
}
